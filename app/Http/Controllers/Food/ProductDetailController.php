<?php

namespace App\Http\Controllers\Food;

use App\Http\Controllers\Controller;
use App\Models\Food\Order;
use App\Models\Food\Order_details;
use App\Models\Food\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductDetailController extends Controller
{


    public function detailFood($id){
        $product = Product::find($id);
//        $product = DB::table('products')
//            ->select('products.price','products.product_name','products.category_id','products.price_img','products.id')
//            ->join('categories','categories.id','=','products.category_id')
//            ->where('categories.category_name','dink')
//            ->get();
        return view('product_detail.food',compact('product'));
    }
    public function orderHistory(){
        $order = DB::table('users')
            ->select('orders.total_price','orders.total_qty','users.name','users.id','products.price','products.product_name','products.category_id','products.price_img','products.id','order_details.payment_status')
            ->join('orders','orders.user_id','=','users.id')
            ->join('products','products.id','=','orders.product_id')
            ->join('order_details','order_details.id','=','orders.id')
            ->where('users.id',Auth::user()->id)
            ->get();
        return view('food.order-history',compact('order'));
    }

    public function detailOrderFood(Request $request,$product_id){

        $product = Product::find($product_id);
        $order = new Order();
        $order->user_id = Auth::user()->id;
        $order->product_id = $product->id;
        $order->total_price = $request->total_price;
        $order->total_qty = $request->total_qty;
        if ($order->save()){
            $order_detail = new Order_details();
            $order_detail->order_id = $order->id;
            $order_detail->qty = $request->total_qty;
            $order_detail->g_totoal = $request->total_price;
            $order_detail->payment_status = $request->payment_status;
            $order_detail->save();
        }


        return view('product_detail.food_list',compact('product'));
    }




    public function detailDrink($id){
        $product = Product::find($id);
        return view('product_detail.drink',compact('product'));
    }

    public function detailDessert($id){
        $product = Product::find($id);
        return view('product_detail.dessert',compact('product'));
    }



    public function checkout(){
        $order_product_detail = \Illuminate\Support\Facades\DB::table('orders')
            ->select('orders.total_price','orders.total_qty','orders.product_id','order_details.payment_status')
            ->join('order_details','order_details.order_id','=','orders.id')
            ->where('order_details.payment_status','unpaid')
            ->get();

        $count_card = \Illuminate\Support\Facades\DB::table('orders')
            ->select('orders.id')
           ->join('order_details','order_details.order_id','=','orders.id')
            ->where('order_details.payment_status','unpaid')
            ->count('orders.id');


        return view('food.checkout',compact('order_product_detail','count_card'));
    }


    //Search

    public function search(Request $request){
        $product = Product::all();

        $input_search = $request->input_text;
        $search = DB::table('products')
            ->select('categories.category_name','products.price_img','products.product_name','products.id')
            ->join('categories','categories.id','=','products.category_id')
            ->where('categories.category_name', 'like', '%'.$input_search.'%')
//            ->orWhere('products.pro_name', 'like', '%'.$input_search.'%')
//            ->orWhere('products.color', 'like', '%'.$input_search.'%')
//            ->orWhere('products.size', 'like', '%'.$input_search.'%')
//            ->orWhere('products.brand', 'like', '%'.$input_search.'%')
//            ->orWhere('phone', 'like', '%'.$input_search.'%')
            ->get();
//        ម្ហូប ភេសជ្ជះ បង្អែម
        if ($search[0]->category_name == 'food'){
            return view('food.food',compact('product'));
        }
        elseif($search[0]->category_name == 'drink'){
            return view('food.drink',compact('product'));
        }
        elseif ($search[0]->category_name == 'dessert'){
            return view('food.dessert',compact('product'));
        }
        else{
            return "hello";
        }
    }


}
