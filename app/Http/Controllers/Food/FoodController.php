<?php

namespace App\Http\Controllers\Food;

use App\Http\Controllers\Controller;
use App\Models\Food\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FoodController extends Controller
{

    public function food(){
       $product = DB::table('products')
            ->select('products.price','products.product_name','products.category_id','products.price_img','products.id')
            ->join('categories','categories.id','=','products.category_id')
            ->where('categories.category_name','food')
            ->get();
        return view('food.food',compact('product'));

    }
    public function drink(){
        $product = DB::table('products')
            ->select('products.price','products.product_name','products.category_id','products.price_img','products.id')
            ->join('categories','categories.id','=','products.category_id')
            ->where('categories.category_name','drink')
            ->get();
        return view('food.drink',compact('product'));

    }
    public function dessert(){
        $product = DB::table('products')
            ->select('products.price','products.product_name','products.category_id','products.price_img','products.id')
            ->join('categories','categories.id','=','products.category_id')
            ->where('categories.category_name','dessert')
            ->get();
        return view('food.dessert',compact('product'));

    }
    public function contact(){
        return view('food.contact');

    }

}
