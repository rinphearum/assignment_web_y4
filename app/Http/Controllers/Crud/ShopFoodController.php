<?php

namespace App\Http\Controllers\Crud;

use App\Http\Controllers\Controller;
use App\Models\Food\Category;
use App\Models\Food\Product;
use Illuminate\Http\Request;

class ShopFoodController extends Controller
{

    //Category

    public function indecCategories(){
        $page = 'index';
        $categories = Category::all();
        return view('crud.categories',compact('page','categories'));
    }

    public function createCategories(){
        $page = 'create';
        return view('crud.categories',compact('page'));
    }

    public function editCategories($id){
        $page = 'edit';
        $category = Category::find($id);
        return view('crud.categories',compact('page','category'));
    }

    public function storeCategories(Request $request){
        $category = new Category();
        $category->category_name = $request->category_name;
        $category->save();
        return redirect('admin/index-categories');
    }
    public function updateCategories(Request $request,$id){
        $category = Category::find($id);
        $category->category_name = $request->category_name;
        $category->save();
        return redirect('/admin/index-categories');
    }
    public function destoyCategories($id){
        Category::destroy($id);
        return redirect()->back();
    }


    //Product

    public function indexProduct(){
        $page = 'index';
        $product = Product::all();
        return view('crud.product',compact('page','product'));
    }

    public function createProduct(){
        $page = 'create';
        $categories = Category::all();
        return view('crud.product',compact('page','categories'));
    }

    public function storeProduct(Request $request){

        if($request->hasFile('price_img')){
            $file= $request->file('price_img');
            $filename= $file->getClientOriginalName();
            $file->move(public_path('asset'), $filename);
        }
        $product = new Product();
        $product->category_id = $request->category_id;
        $product->price_img = $filename;
        $product->product_name = $request->product_name;
        $product->price = $request->price;
        $product->unit_price = $request->unit_price;
        $product->unit_in_stock = $request->unit_in_stock;
        $product->save();
        return redirect('admin/index-product');
    }
    public function editProduct($id){
        $page = 'edit';
        return redirect('admin/index-product');
    }
    public function updateProduct($id){
        $category = Product::destroy($id);
        return redirect()->back();
    }
    public function destoyProduct($id){
        $category = Product::destroy($id);
        return redirect()->back();
    }

    public function listCustomer(){
        return view('user-admin.menu.customers');
    }



}
