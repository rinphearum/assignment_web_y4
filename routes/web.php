<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Food\FoodController;
use App\Http\Controllers\Crud\ShopFoodController;
use \App\Http\Controllers\Food\ProductDetailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('food.home');
});

Route::get('/food',[FoodController::class,'food']);
Route::get('/drink',[FoodController::class,'drink']);
Route::get('/dessert',[FoodController::class,'dessert']);
Route::get('/contact',[FoodController::class,'contact']);
Route::get('/order-history',[ProductDetailController::class,'orderHistory']);

//checkout
Route::get('/checkout',[ProductDetailController::class,'checkout']);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';



//Food



//Product Detail

Route::get('product-detail/{id}',[ProductDetailController::class,'detailFood']);
Route::get('product-detail-drink/{id}',[ProductDetailController::class,'detailDrink']);
Route::get('product-detail-dessert/{id}',[ProductDetailController::class,'detailDessert']);

Route::get('product-detail/find-product-detail/{product_id}',[ProductDetailController::class,'detailOrderFood']);

Route::get('find-category',[ProductDetailController::class,'search']);






