<?php

use App\Http\Controllers\Crud\ShopFoodController;
use Illuminate\Support\Facades\Route;

Route::get('/admin',[\App\Http\Controllers\Admin\AdminController::class,'Dashborad']);
Route::get('/dashboard',[\App\Http\Controllers\Admin\AdminController::class,'DashboradAdmin']);
Route::get('/',[\App\Http\Controllers\Admin\AdminController::class,'DashboradAdmin']);

Route::get('/dashboard-drink',function (){
    return view('user-admin.dashborad_drink');
});
Route::get('/dashboard-dessert',function (){
    return view('user-admin.dashborad_dessert');
});

//Categories

Route::get('index-categories',[ShopFoodController::class,'indecCategories']);
Route::get('create-categories',[ShopFoodController::class,'createCategories']);
Route::post('store-categories',[ShopFoodController::class,'storeCategories']);
Route::get('category-edit/{id}',[ShopFoodController::class,'editCategories']);
Route::put('categories-update/{id}',[ShopFoodController::class,'updateCategories']);
Route::get('category-delete/{id}',[ShopFoodController::class,'destoyCategories']);

//Product


Route::get('index-product',[ShopFoodController::class,'indexProduct']);
Route::get('create-product',[ShopFoodController::class,'createProduct']);
Route::post('store-product',[ShopFoodController::class,'storeProduct']);
Route::get('destroy-product/{id}',[ShopFoodController::class,'destoyProduct']);

Route::get('list-customer',[ShopFoodController::class,'listCustomer']);
Route::get('list-order',function (){
    return view('user-admin.menu.orders');
});
