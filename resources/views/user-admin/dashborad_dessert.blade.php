@extends('user-admin.layout')
@section('admin-html')
@php
    $total_customer =  \App\Models\User::where('user_role_type','customer')->count('id');
    $total_user =  \App\Models\User::count('id');
    $total_order =   \Illuminate\Support\Facades\DB::table('products')
            ->select('orders.total_qty','orders.total_price','products.price','products.product_name','products.category_id','products.price_img','products.id')
            ->join('categories','categories.id','=','products.category_id')
            ->join('orders','product_id','=','products.id')
            ->where('categories.category_name','dessert')
            ->count('orders.id');;

@endphp
<h1 style="color: white;margin-bottom: 30px">Dessert</h1>

    <div class="col-div-3">
        <div class="box">
            <p>{{$total_customer}}<br/><span>Total Customers</span></p>
            <i class="fa fa-users box-icon"></i>
        </div>
    </div>
    <div class="col-div-3">
        <div class="box">
            <p>{{$total_user}}<br/><span>Total User</span></p>
            <i class="fa fa-list box-icon"></i>
        </div>
    </div>
    <div class="col-div-3">
        <div class="box">
            <p>{{$total_order}}<br/><span>Total Orders</span></p>
            <i class="fa fa-shopping-bag box-icon"></i>
        </div>
    </div>
    <div class="col-div-3">
        <div class="box">
            <p>78<br/><span>Total Income</span></p>
            <i class="fa fa-tasks box-icon"></i>
        </div>
    </div>
    <div class="clearfix"></div>
    <br/><br/>

@php
    $best_selling_product =  \Illuminate\Support\Facades\DB::table('products')
            ->select('orders.total_qty','orders.total_price','products.price','products.product_name','products.category_id','products.price_img','products.id')
            ->join('categories','categories.id','=','products.category_id')
            ->join('orders','product_id','=','products.id')
            ->where('categories.category_name','dessert')
            ->orderBy('orders.created_at','desc')
            ->paginate(5);
@endphp
<div class="col-div-8">
    <div class="box-8">
        <div class="content-box">
            <p>Best Selling Product</p>
            <br/>
            <table>
                <tr>
                    <th>Name</th>
                    <th>Total Qty</th>
                    <th>Total Price</th>
                </tr>
                @foreach($best_selling_product as $best_sell)
                    <tr>
                        <td>{{$best_sell->product_name}}</td>
                        <td>{{$best_sell->total_qty}}</td>
                        <td>${{$best_sell->price}}</td>
                    </tr>
                @endforeach



            </table>
        </div>
    </div>
</div>

{{--    <div class="col-div-4">--}}
{{--        <div class="box-4">--}}
{{--            <div class="content-box">--}}
{{--                <p>Total Sale</p>--}}

{{--                <div class="circle-wrap">--}}
{{--                    <div class="circle">--}}
{{--                        <div class="mask full">--}}
{{--                            <div class="fill"></div>--}}
{{--                        </div>--}}
{{--                        <div class="mask half">--}}
{{--                            <div class="fill"></div>--}}
{{--                        </div>--}}
{{--                        <div class="inside-circle"> 70% </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class="clearfix"></div>
@endsection
