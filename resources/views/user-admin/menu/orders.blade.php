@extends('user-admin.layout')
@section('admin-html')
    <style>
        @import {{url("https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap")}};
        body {
            font-family: "Roboto", sans-serif;
        }

        .main-content {
            padding-bottom: 100px;
        }

        .table {
            border-spacing: 0 15px;
            border-collapse: separate;
        }
        .table thead tr th{
            color: red;
        }
        .table thead tr th,
        .table thead tr td,
        .table tbody tr th,
        .table tbody tr td {
            vertical-align: middle;
            border: none;
            /*border-right: 1px solid white;*/
            /*border-left: 1px solid white;*/
        }
        .table thead tr th:nth-last-child(1),
        .table thead tr td:nth-last-child(1),
        .table tbody tr th:nth-last-child(1),
        .table tbody tr td:nth-last-child(1) {
            text-align: center;
        }
        .table tbody tr {
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
            border-radius: 5px;
        }
        .table tbody tr td {
            /*background: #fff;*/
        }
        .table tbody tr td:nth-child(1) {
            border-radius: 5px 0 0 5px;
        }
        .table tbody tr td:nth-last-child(1) {
            border-radius: 0 5px 5px 0;
        }

        .user-info {
            display: flex;
            align-items: center;
        }
        .user-info__img img {
            margin-right: 15px;
            height: 55px;
            width: 55px;
            border-radius: 45px;
            border: 3px solid #fff;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }

        .active-circle {
            height: 10px;
            width: 10px;
            border-radius: 10px;
            margin-right: 5px;
            display: inline-block;
        }

    </style>
    1234567
    <section class="main-content">
        <div class="container">
            {{--                <a href="{{url('admin/create-product')}}" style="text-decoration: none;"><p style="display: inline-block">+ Add New</p></a>--}}
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Product Name</th>
                    <th>Type</th>
                    <th>Total Price</th>
                    <th>Payment Status</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                @php
                    $order = \Illuminate\Support\Facades\DB::select('
                        SELECT
                        products.category_id,
                        products.product_name,
                        order_details.g_totoal,
                        order_details.id,
                        order_details.payment_status,
                        orders.user_id,
                        orders.total_price
                        FROM
                        orders
                        INNER JOIN
                        products
                        ON
                        orders.product_id = products.id
                        INNER JOIN
                        order_details
                        ON
                        order_details.order_id = orders.id
                        ORDER BY order_details.created_at DESC

                    ');
                @endphp
                @foreach($order as $pro)
                    @php
                        $user = \App\Models\User::find(optional($pro)->user_id);
                        $product = \App\Models\Food\Category::find(optional($pro)->category_id);
                    @endphp


                    <tr class="alert" role="alert">
                        <td>{{$pro->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$pro->product_name}}</td>
                        <td>{{$product->category_name}}</td>
                        <td>${{$pro->total_price}}</td>
                        @if($pro->payment_status == 'unpaid')
                        <td style="color: darkred;font-weight: bold">{{$pro->payment_status}}</td>
                        @else
                            <td style="color: #45a049">{{$pro->payment_status}}</td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>



@endsection
