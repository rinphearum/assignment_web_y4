@extends('user-admin.layout')
@section('admin-html')
        <style>
            @import {{url("https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap")}};
            body {
                font-family: "Roboto", sans-serif;
            }

            .main-content {
                padding-bottom: 100px;
            }

            .table {
                border-spacing: 0 15px;
                border-collapse: separate;
            }
            .table thead tr th{
                color: red;
            }
            .table thead tr th,
            .table thead tr td,
            .table tbody tr th,
            .table tbody tr td {
                vertical-align: middle;
                border: none;
                /*border-right: 1px solid white;*/
                /*border-left: 1px solid white;*/
            }
            .table thead tr th:nth-last-child(1),
            .table thead tr td:nth-last-child(1),
            .table tbody tr th:nth-last-child(1),
            .table tbody tr td:nth-last-child(1) {
                text-align: center;
            }
            .table tbody tr {
                box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
                border-radius: 5px;
            }
            .table tbody tr td {
                /*background: #fff;*/
            }
            .table tbody tr td:nth-child(1) {
                border-radius: 5px 0 0 5px;
            }
            .table tbody tr td:nth-last-child(1) {
                border-radius: 0 5px 5px 0;
            }

            .user-info {
                display: flex;
                align-items: center;
            }
            .user-info__img img {
                margin-right: 15px;
                height: 55px;
                width: 55px;
                border-radius: 45px;
                border: 3px solid #fff;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            }

            .active-circle {
                height: 10px;
                width: 10px;
                border-radius: 10px;
                margin-right: 5px;
                display: inline-block;
            }

        </style>
        <section class="main-content">
            <div class="container">
{{--                <a href="{{url('admin/create-product')}}" style="text-decoration: none;"><p style="display: inline-block">+ Add New</p></a>--}}
                <table class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>User Role</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    @php
                        $customer = \App\Models\User::where('user_role_type','customer')->paginate(5);
                    @endphp
                    @foreach($customer as $pro)

                        <tr class="alert" role="alert">
                           <td>{{$pro->id}}</td>
                           <td>{{$pro->name}}</td>
                            <td>{{$pro->phone}}</td>
                            <td>{{$pro->address}}</td>
                            <td style="color: #45a049">{{$pro->user_role_type}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>



@endsection
