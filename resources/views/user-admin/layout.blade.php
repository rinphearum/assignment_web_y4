<!Doctype HTML>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="css/style.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<style>
    body{
        margin:0;
        padding: 0;
        background-color:#1b203d;
        overflow: hidden;
        font-family: system-ui;
    }
    .clearfix{
        clear: both;
    }
    .logo{
        font-weight: bold;
        color: white;
        margin: 0 0 30px 20px;
    }
    .logo span{
        color: #f7403b;
    }
    .sidenav {
        height: 100%;
        width: 200px;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: #272c4a;
        overflow: hidden;
        transition: 0.5s;
        padding-top: 30px;
    }
    .sidenav a {
        padding: 15px 0 10px 17px;
        text-decoration: none;
        font-size: 17px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }
    .sidenav a:hover {
        color: #f1f1f1;
        background-color:#1b203d;
    }
    .sidenav{
        position: absolute;
        top: 0;
        right: 25px;
        font-size: 36px;
    }
    #main {
        transition: margin-left .5s;
        padding: 16px;
        margin-left: 200px;
    }
    .head{
        padding:30px;
    }
    .col-div-6{
        width: 50%;
        float: left;
    }
    .profile{
        display: inline-block;
        float: right;
        width: 200px;
    }
    /*.pro-img{*/
    /*    float: left;*/
    /*    width: 40px;*/
    /*    margin-top: 5px;*/
    /*}*/
    .profile p{
        color: white;
        font-weight: 500;
        margin-left: 45px;
        font-size: 13.5px;
    }
    .profile p span{
        font-weight: 400;
        font-size: 12px;
        display: block;
        color: #8e8b8b;
    }
    .col-div-3{
        width: 25%;
        float: left;
    }
    .box{
        width: 85%;
        height: 90px;
        background-color: #272c4a;
        margin-left: 10px;
        padding:10px;
    }
    .box p{
        font-size: 20px;
        color: white;
        font-weight: bold;
        line-height: 30px;
        padding-left: 10px;
        margin-top: 8px;
        display: inline-block;
    }
    .box p span{
        font-size: 17px;
        font-weight: 400;
        color: #818181;
    }
    .box-icon{
        font-size: 17px!important;
        float: right;
        margin-top: 35px!important;
        color: #818181;
        padding-right: 10px;
    }
    .col-div-8{
        width: 100%;
        float: left;
    }
    .col-div-4{
        width: 30%;
        float: left;
    }
    .content-box{
        padding: 10px;
    }
    .content-box p{
        margin: 0;
        font-size: 17px;
        color: #f7403b;
    }
    .content-box p span{
        float: right;
        background-color: #ddd;
        padding: 10px 20px;
        font-size: 15px;
    }
    .box-8, .box-4{
        width: 100%;
        background-color: #272c4a;
        height: 300px;

    }
    .nav2{
        display: none;
    }

    .box-8{
        margin-left: 10px;
    }
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;

    }
    td, th {
        text-align: left;
        padding:10px;
        color: #ddd;
        border-bottom: 1px solid #81818140;
    }
    td{
        font-size: 16px;
    }
    .circle-wrap {
        margin: 20px auto;
        width: 110px;
        height: 110px;
        background: #e6e2e7;
        border-radius: 50%;
    }
    .circle-wrap .circle .mask,
    .circle-wrap .circle .fill {
        width: 110px;
        height: 110px;
        position: absolute;
        border-radius: 50%;
    }
    .circle-wrap .circle .mask {
        clip: rect(0px, 180px, 280px, 79px);
    }

    .circle-wrap .circle .mask .fill {
        clip: rect(0px, 280px, 790px, 10px);
        background-color: #f7403b;
    }
    .circle-wrap .circle .mask.full,
    .circle-wrap .circle .fill {
        animation: fill ease-in-out 3s;
        transform: rotate(100deg);
    }

    @keyframes fill {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(100deg);
        }
    }
    .circle-wrap .inside-circle {
        width: 95px;
        height: 95px;
        border-radius: 50%;
        background: #fff;
        line-height: 100px;
        text-align: center;
        margin-left: 7px;
        position: absolute;
        z-index: 100;
        margin-top: 6px;
        font-weight: 500;
        font-size: 2em;
    }
</style>

<body>

<div id="mySidenav" class="sidenav">
    <p style="font-size: 19px" class="logo"><span>M</span>-SoftTech</p>
    <a href="{{url('admin/dashboard')}}" class="icon-a"><i class="fa fa-dashboard icons"></i> &nbsp;&nbsp;Food</a>
    <a href="{{url('admin/dashboard-drink')}}" class="icon-a"><i class="fa fa-dashboard icons"></i> &nbsp;&nbsp;Drink</a>
    <a href="{{url('admin/dashboard-dessert')}}" class="icon-a"><i class="fa fa-dashboard icons"></i> &nbsp;&nbsp;Dessert</a>
    <a href="{{url('admin/list-customer')}}"class="icon-a"><i class="fa fa-users icons"></i> &nbsp;&nbsp;Customers</a>
    <a href="{{url('admin/index-categories')}}"class="icon-a"><i class="fa fa-list icons"></i> &nbsp;&nbsp;Categories</a>
    <a href="{{url('admin/index-product')}}"class="icon-a"><i class="fa fa-shopping-bag icons"></i> &nbsp;&nbsp;Product</a>
    <a href="{{url('admin/list-order')}}"class="icon-a"><i class="fa fa-shopping-bag icons"></i> &nbsp;&nbsp;Orders</a>
    <form action="{{route('logout')}}" method="POST">
        @csrf
        <button  style="border: none;cursor: pointer;font-size: 17px;background: none" type="submit"><span style="color: red">Logout</span></button>
    </form>
</div>
<div id="main">

    <div class="head">
        <div class="col-div-6">
            <span style="font-size:19px;cursor:pointer; color: white;" class="nav"  >&#9776; Dashboard</span>
            <span style="font-size:19px;cursor:pointer; color: white;" class="nav2"  >&#9776; Dashboard</span>
        </div>


{{--        <div class="clearfix"></div>--}}
    </div>

{{--    <div class="clearfix"></div>--}}
{{--    <br/>--}}
@yield('admin-html')
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>

    $(".nav").click(function(){
        $("#mySidenav").css('width','70px');
        $("#main").css('margin-left','70px');
        $(".logo").css('visibility', 'hidden');
        $(".logo span").css('visibility', 'visible');
        $(".logo span").css('margin-left', '-10px');
        $(".icon-a").css('visibility', 'hidden');
        $(".icons").css('visibility', 'visible');
        $(".icons").css('margin-left', '-8px');
        $(".nav").css('display','none');
        $(".nav2").css('display','block');
    });

    $(".nav2").click(function(){
        $("#mySidenav").css('width','210px');
        $(".logo span").css('margin-left', '10px');
        $("#main").css('margin-left','210px');
        $(".logo").css('visibility', 'visible');
        $(".icon-a").css('visibility', 'visible');
        $(".icons").css('visibility', 'visible');
        $(".icons").css('margin-left', '1px');
        $(".nav").css('display','block');
        $(".nav2").css('display','none');
    });

</script>

</body>


</html>
