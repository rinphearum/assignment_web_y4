@extends('user-admin.layout')
@section('admin-html')
@if($page == 'index')
    <style>
        @import {{url("https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap")}};
        body {
            font-family: "Roboto", sans-serif;
        }

        .main-content {
            padding-bottom: 100px;
        }

        .table {
            border-spacing: 0 15px;
            border-collapse: separate;
        }
        .table thead tr th{
            color: red;
        }
        .table thead tr th,
        .table thead tr td,
        .table tbody tr th,
        .table tbody tr td {
            vertical-align: middle;
            border: none;
            /*border-right: 1px solid white;*/
            /*border-left: 1px solid white;*/
        }
        .table thead tr th:nth-last-child(1),
        .table thead tr td:nth-last-child(1),
        .table tbody tr th:nth-last-child(1),
        .table tbody tr td:nth-last-child(1) {
            text-align: center;
        }
        .table tbody tr {
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
            border-radius: 5px;
        }
        .table tbody tr td {
            /*background: #fff;*/
        }
        .table tbody tr td:nth-child(1) {
            border-radius: 5px 0 0 5px;
        }
        .table tbody tr td:nth-last-child(1) {
            border-radius: 0 5px 5px 0;
        }

        .user-info {
            display: flex;
            align-items: center;
        }
        .user-info__img img {
            margin-right: 15px;
            height: 55px;
            width: 55px;
            border-radius: 45px;
            border: 3px solid #fff;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }

        .active-circle {
            height: 10px;
            width: 10px;
            border-radius: 10px;
            margin-right: 5px;
            display: inline-block;
        }

    </style>
<section class="main-content">
    <div class="container">
        <a href="{{url('admin/create-product')}}" style="text-decoration: none;"><p style="display: inline-block">+ Add New</p></a>
        <table class="table">
            <thead>
            <tr>
                <th>Image</th>
                <th>Product Name</th>
                <th>Category</th>
                <th>Product Prcie</th>
                <th>Unit Price</th>
                <th>Unit In Stock</th>
                <th style="display: flex;justify-content: center">Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            @foreach($product as $pro)
                @php
                    $category = \App\Models\Food\Category::find(optional($pro)->category_id);
                @endphp
                <tr class="alert" role="alert">
                    <td><img style="width: 40px;height: 40px;background-repeat: no-repeat;background-position: center;background-size: cover" src="{{url('/asset/'.$pro->price_img)}}"></td>
                    <td>{{$pro->pro_name}}</td>
                    <td>{{$category->category_name}}</td>
                    <td>{{$pro->price}}</td>
                    <td>{{$pro->unit_price}}</td>
                    <td>{{$pro->unit_in_stock}}</td>
                    <td>
                        <div class="dropdown open">
                            <a href="#!" class="px-2" id="triggerId1" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">
                                <i class="fa fa-ellipsis-v"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="triggerId1">
                                <a class="dropdown-item" href="#"><i class="fa fa-pencil mr-1"></i> Edit</a>
                                <a class="dropdown-item text-danger" href="#"><i class="fa fa-trash mr-1"></i> Delete</a>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>

@elseif($page == 'create')
    <div class="container">
        <div class="title">
            <h2>Create Form Category</h2>
        </div>
        <div class="d-flex">
            <form action="{{url('admin/store-product')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <label>
                    <span class="fname">Image<span class="required">*</span></span>
                    <input type="file" name="price_img">
                </label>
                <label>
                    <span class="fname">Product Name<span class="required">*</span></span>
                    <input type="text" name="product_name">
                </label>
                <label>
                    <span class="fname">Unit Price<span class="required">*</span></span>
                    <input type="number" name="unit_price">
                </label>
                <label>
                    <span class="fname">Price<span class="required">*</span></span>
                    <input type="number" name="price">
                </label>
                <label>
                    <span class="fname">Unit In Stock<span class="required">*</span></span>
                    <input type="number" name="unit_in_stock">
                </label>
                <label>
                    <span>Catgories <span class="required">*</span></span>
                    <select name="category_id" id="">
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->category_name}}</option>
                        @endforeach
                    </select>
                </label>
                <button type="submit">Save</button>
            </form>
        </div>
    </div>
    <style>
        @import {{url('https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700')}};

        body{
            background: {{url('http://all4desktop.com/data_images/original/4236532-background-images.jpg')}};
            font-family: 'Roboto Condensed', sans-serif;
            color: #262626;
            margin: 5% 0;
        }
        .container{
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }
        @media (min-width: 1200px)
        {
            .container{
                max-width: 1140px;
            }
        }
        .d-flex{
            display: flex;
            flex-direction: row;
            background: #f6f6f6;
            border-radius: 0 0 5px 5px;
            padding: 25px;
        }
        form{
            flex: 4;
        }
        .Yorder{
            flex: 2;
        }
        .title{
            background: -webkit-gradient(linear, left top, right bottom, color-stop(0, #5195A8), color-stop(100, #70EAFF));
            background: -moz-linear-gradient(top left, #5195A8 0%, #70EAFF 100%);
            background: -ms-linear-gradient(top left, #5195A8 0%, #70EAFF 100%);
            background: -o-linear-gradient(top left, #5195A8 0%, #70EAFF 100%);
            background: linear-gradient(to bottom right, #5195A8 0%, #70EAFF 100%);
            border-radius:5px 5px 0 0 ;
            padding: 20px;
            color: #f6f6f6;
        }
        h2{
            margin: 0;
            padding-left: 15px;
        }
        .required{
            color: red;
        }
        label, table{
            display: block;
            margin: 15px;
        }
        label>span{
            float: left;
            width: 25%;
            margin-top: 12px;
            padding-right: 10px;
        }
        input[type="text"], input[type="tel"],input[type="number"], input[type="email"],input[type="file"], select
        {
            width: 70%;
            height: 35px;
            padding: 5px 10px;
            margin-bottom: 10px;
            border: 1px solid #dadada;
            color: #888;
        }
        select{
            width: 70%;
            height: 35px;
            padding: 5px 10px;
            margin-bottom: 10px;
        }
        .Yorder{
            margin-top: 15px;
            height: 600px;
            padding: 20px;
            border: 1px solid #dadada;
        }
        table{
            margin: 0;
            padding: 0;
        }
        th{
            border-bottom: 1px solid #dadada;
            padding: 10px 0;
        }
        tr>td:nth-child(1){
            text-align: left;
            color: #2d2d2a;
        }
        tr>td:nth-child(2){
            text-align: right;
            color: #52ad9c;
        }
        td{
            border-bottom: 1px solid #dadada;
            padding: 25px 25px 25px 0;
        }

        p{
            display: block;
            color: #888;
            margin: 0;
            padding-left: 25px;
        }
        .Yorder>div{
            padding: 15px 0;
        }
        button{
            width: 10%;
            float: right;
            margin-top: 10px;
            padding: 10px;
            border: none;
            border-radius: 30px;
            background: #52ad9c;
            color: #fff;
            font-size: 15px;
            font-weight: bold;
        }
        button:hover{
            cursor: pointer;
            background: #428a7d;
        }
    </style>

@elseif($page == 'edit')
    <div class="container">
        <div class="title">
            <h2>Create Form Category</h2>
        </div>
        <div class="d-flex">
            <form action="{{url('store-categories')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <label>
                    <span class="fname">Image<span class="required">*</span></span>
                    <input type="file" name="price_image">
                </label>
                <label>
                    <span class="fname">Product Name<span class="required">*</span></span>
                    <input type="number" name="pro_name">
                </label>
                <label>
                    <span class="fname">Unit Price<span class="required">*</span></span>
                    <input type="number" name="unit_price">
                </label>
                <label>
                    <span class="fname">Price<span class="required">*</span></span>
                    <input type="number" name="price">
                </label>
                <label>
                    <span class="fname">Unit In Stock<span class="required">*</span></span>
                    <input type="number" name="unit_in_stock">
                </label>
                <label>
                    <span>Catgories <span class="required">*</span></span>
                    <select name="category_id" id="">
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->category_name}}</option>
                        @endforeach
                    </select>
                </label>
                <button type="submit">Save</button>
            </form>
        </div>
    </div>
    <style>
        @import {{url('https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700')}};

        body{
            background: {{url('http://all4desktop.com/data_images/original/4236532-background-images.jpg')}};
            font-family: 'Roboto Condensed', sans-serif;
            color: #262626;
            margin: 5% 0;
        }
        .container{
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }
        @media (min-width: 1200px)
        {
            .container{
                max-width: 1140px;
            }
        }
        .d-flex{
            display: flex;
            flex-direction: row;
            background: #f6f6f6;
            border-radius: 0 0 5px 5px;
            padding: 25px;
        }
        form{
            flex: 4;
        }
        .Yorder{
            flex: 2;
        }
        .title{
            background: -webkit-gradient(linear, left top, right bottom, color-stop(0, #5195A8), color-stop(100, #70EAFF));
            background: -moz-linear-gradient(top left, #5195A8 0%, #70EAFF 100%);
            background: -ms-linear-gradient(top left, #5195A8 0%, #70EAFF 100%);
            background: -o-linear-gradient(top left, #5195A8 0%, #70EAFF 100%);
            background: linear-gradient(to bottom right, #5195A8 0%, #70EAFF 100%);
            border-radius:5px 5px 0 0 ;
            padding: 20px;
            color: #f6f6f6;
        }
        h2{
            margin: 0;
            padding-left: 15px;
        }
        .required{
            color: red;
        }
        label, table{
            display: block;
            margin: 15px;
        }
        label>span{
            float: left;
            width: 25%;
            margin-top: 12px;
            padding-right: 10px;
        }
        input[type="text"], input[type="tel"],input[type="number"], input[type="email"],input[type="file"], select
        {
            width: 70%;
            height: 35px;
            padding: 5px 10px;
            margin-bottom: 10px;
            border: 1px solid #dadada;
            color: #888;
        }
        select{
            width: 70%;
            height: 35px;
            padding: 5px 10px;
            margin-bottom: 10px;
        }
        .Yorder{
            margin-top: 15px;
            height: 600px;
            padding: 20px;
            border: 1px solid #dadada;
        }
        table{
            margin: 0;
            padding: 0;
        }
        th{
            border-bottom: 1px solid #dadada;
            padding: 10px 0;
        }
        tr>td:nth-child(1){
            text-align: left;
            color: #2d2d2a;
        }
        tr>td:nth-child(2){
            text-align: right;
            color: #52ad9c;
        }
        td{
            border-bottom: 1px solid #dadada;
            padding: 25px 25px 25px 0;
        }

        p{
            display: block;
            color: #888;
            margin: 0;
            padding-left: 25px;
        }
        .Yorder>div{
            padding: 15px 0;
        }
        button{
            width: 10%;
            float: right;
            margin-top: 10px;
            padding: 10px;
            border: none;
            border-radius: 30px;
            background: #52ad9c;
            color: #fff;
            font-size: 15px;
            font-weight: bold;
        }
        button:hover{
            cursor: pointer;
            background: #428a7d;
        }
    </style>
@endif

@endsection
