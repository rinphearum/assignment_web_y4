@extends('layout.layout')
@section('html')
<style>
    .button {
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
    .button2 {background-color: #008CBA;} /* Blue */
    .button3 {background-color: #f44336;} /* Red */
    .button4 {background-color: #e7e7e7; color: black;} /* Gray */
    .button5 {background-color: #555555;} /* Black */
</style>
<img style="width: 100%" src="{{url("/asset/img_1.png")}}">
<h1 class="heading" style="margin-top: 50px"> <span>Categories</span> </h1>
<center>
    <div style="width: 80%;display: flex;margin:50px" >
        <a  style="width: 30%;height: 400px;border-style: groove;display: inline-block;margin: 10px" href="{{url('/food')}}">
        <div>
            <div style="width: 100%;height: 300px">
                <img src="{{url("/asset/img_1.png")}}" style="width: 100%;height: 100%;background-size: cover;background-position: center">
            </div>
            <div style="width: 100%;height: 100px;display:flex;flex-direction: column;justify-content: center ">
                <button class="button">Food</button>
            </div>
        </div>
        </a>
        <a  style="width: 30%;height: 400px;border-style: groove;display: inline-block;margin: 10px" href="{{url('/drink')}}">
            <div>
            <div style="width: 100%;height: 300px">
                <img src="{{url("/asset/img_4.png")}}" style="width: 100%;height: 100%;background-size: cover;background-position: center">
            </div>
            <div style="width: 100%;height: 100px;display:flex;flex-direction: column;justify-content: center ">
                <button class="button">Drink</button>
            </div>
        </div>
        </a>
        <a style="width: 30%;height: 400px;border-style: groove;display: inline-block;margin: 10px" href="{{url('/dessert')}}">
        <div >
            <div style="width: 100%;height: 300px">
                <img src="{{url("/asset/img_3.png")}}" style="width: 100%;height: 100%;background-size: cover;background-position: center">
            </div>
            <div style="width: 100%;height: 100px;display:flex;flex-direction: column;justify-content: center ">
                <button class="button">Dessert</button>
            </div>
        </div>
        </a>
    </div>
</center>

@endsection
