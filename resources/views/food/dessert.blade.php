@extends('layout.layout')
@section('html')

    <section class="featured" id="featured">
        <h1 class="heading"> <span>មឺនុយបង្អែម</span> </h1>
    </section>
    <section class="deal">

        <div class="food-menu">
            @foreach($product as $pro)
                <div class="food">
                    <a href="{{url('product-detail/'.$pro->id)}}"> <div class="img" style="background-image: url({{asset('asset/'.$pro->price_img)}})">

                        </div>
                    </a>
                    <div class="txt">
                        <p>Name: <b>{{$pro->product_name}}</b></p>
                        <p>Price: <b>${{$pro->price}}</b></p>
                    </div>
                </div>
            @endforeach
        </div>

    </section>

@endsection
