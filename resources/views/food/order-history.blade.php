@extends('layout.layout')
@section('html')
    <!DOCTYPE html>
<html>
<head>
    <style>
        #customers {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #04AA6D;
            color: white;
        }
    </style>
</head>
<body>


<div style="width: 80%;margin-left: auto;margin-right: auto">
    <h1 class="heading" style="margin-top: 50px"> <span>បញ្ជីប្រវត្តិនៃការកុម៉្មង់</span> </h1>
    <table id="customers" style="margin-top: 50px;margin-bottom: 50px;">
        <tr>
            <th>Id</th>
            <th>Product Name</th>
            <th>Total Price</th>
            <th>Total Quantity</th>
            <th>Payment Status</th>
        </tr>
        <tbody>
        @foreach($order as $p)
        <tr>
            <td>{{$p->id}}</td>
            <td>{{$p->product_name}}</td>
            <td>{{$p->total_price}}</td>
            <td>{{$p->total_qty}}</td>

            @if($p->payment_status == 'unpaid')
                <td style="color: darkred;font-weight: bold">{{$p->payment_status}}</td>
            @else
                <td style="color: #45a049">{{$p->payment_status}}</td>
            @endif
        </tr>
        @endforeach
        </tbody>


    </table>
</div>


</body>
</html>



@endsection
