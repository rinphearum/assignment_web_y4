@if(\Illuminate\Support\Facades\Auth::user())
    @if(\Illuminate\Support\Facades\Auth::user()->user_role_type == 'customer')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Khmer Food</title>
    <link rel="icon" href="logo/2.png">

    <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />

    <!-- font awesome cdn link  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

    <!-- custom css file link  -->
    <link rel="stylesheet" href="style.css">

</head>
<style>
    @import url('https://fonts.googleapis.com/css2?family=Battambang:wght@100;300;400;500;600&display=swap');

    :root{
        --green:#27ae60;
        --dark-color:#219150;
        --black:#444;
        --light-color:#666;
        --border:.1rem solid rgba(0,0,0,.1);
        --border-hover:.1rem solid var(--black);
        --box-shadow:0 .5rem 1rem rgba(0,0,0,.1);
    }

    *{
        font-family: 'Poppins', sans-serif,'Battambang';
        /* font-family: Arial,'Battambang', cursive; */
        margin:0; padding:0;
        box-sizing: border-box;
        outline: none; border:none;
        text-decoration: none;
        text-transform: capitalize;
        transition:all .2s linear;
        transition:  none;
    }

    html{
        font-size: 62.5%;
        overflow-x: hidden;
        scroll-padding-top: 5rem;
        scroll-behavior: smooth;
    }

    html::-webkit-scrollbar{
        width:1rem;
    }

    html::-webkit-scrollbar-track{
        background:transparent;
    }

    html::-webkit-scrollbar-thumb{
        background:var(--black);
    }

    section{
        padding:5rem 9%;
    }

    .heading{
        text-align: center;
        margin-bottom: 2rem;
        position: relative;
    }

    .heading::before{
        content: '';
        position: absolute;
        top:50%; left:0;
        transform: translateY(-50%);
        width: 100%;
        height:.01rem;
        background: rgba(0,0,0,.1);
        z-index: -1;
    }

    .heading span{
        font-size: 3rem;
        padding:.5rem 2rem;
        color:var(--black);
        background: #fff;
        border:var(--border);
    }

    .btn{
        margin-top: 1rem;
        display:inline-block;
        padding:.9rem 3rem;
        border-radius: .5rem;
        color:#fff;
        background:var(--green);
        font-size: 1.7rem;
        cursor: pointer;
        font-weight: 500;
    }

    .btn:hover{
        background:var(--dark-color);
    }

    .header .header-1{
        background:#fff;
        padding:1.5rem 9%;
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    .header .header-1 .logo{
        font-size: 2.5rem;
        font-weight: bolder;
        color:var(--black);
    }

    .header .header-1 .logo i{
        color:var(--green);
    }

    .header .header-1 .search-form{
        width:50rem;
        height:5rem;
        border:var(--border);
        overflow: hidden;
        background:#fff;
        display:flex;
        align-items: center;
        border-radius: .5rem;
    }

    .header .header-1 .search-form input{
        font-size: 1.6rem;
        padding:0 1.2rem;
        height:100%;
        width:100%;
        text-transform: none;
        color:var(--black);
    }

    .header .header-1 .search-form label{
        font-size: 2.5rem;
        padding-right: 1.5rem;
        color:var(--black);
        cursor: pointer;
    }

    .header .header-1 .search-form label:hover{
        color:var(--green);
    }

    .header .header-1 .icons div,
    .header .header-1 .icons a{
        font-size: 2.5rem;
        margin-left: 1.5rem;
        color:var(--black);
        cursor: pointer;
    }

    .header .header-1 .icons div:hover,
    .header .header-1 .icons a:hover{
        color:var(--green);
    }

    #search-btn{
        display: none;
    }

    .header .header-2{
        background:var(--green);
    }

    .header .header-2 .navbar{
        text-align: center;
    }

    .header .header-2 .navbar a{
        color:#fff;
        display: inline-block;
        padding:1.2rem;
        font-size: 1.7rem;
    }

    .header .header-2 .navbar a:hover{
        background:var(--dark-color);
    }

    .header .header-2.active{
        position:fixed;
        top:0; left:0; right:0;
        z-index: 1000;

    }

    .bottom-navbar{
        text-align: center;
        background:var(--green);
        position: fixed;
        bottom:0; left:0; right:0;
        z-index: 1000;
        display: none;
    }

    .bottom-navbar a{
        font-size: 2.5rem;
        padding:2rem;
        color:#fff;
    }

    .bottom-navbar a:hover{
        background:var(--dark-color);
    }

    .login-form-container{
        display: flex;
        align-items: center;
        justify-content: center;
        background:rgba(255,255,255,.9);
        position:fixed;
        top:0; right:-105%;
        z-index: 10000;
        height:100%;
        width:100%;
    }

    .login-form-container.active{
        right:0;
    }

    .login-form-container form{
        background:#fff;
        border:var(--border);
        width:40rem;
        padding:2rem;
        box-shadow: var(--box-shadow);
        border-radius: .5rem;
        margin:2rem;
    }

    .login-form-container form h3{
        font-size: 2.5rem;
        text-transform: uppercase;
        color:var(--black);
        text-align: center;
    }

    .login-form-container form span{
        display: block;
        font-size: 1.5rem;
        padding-top: 1rem;
    }

    .login-form-container form .box{
        width: 100%;
        margin:.7rem 0;
        font-size: 1.6rem;
        border:var(--border);
        border-radius: .5rem;
        padding:1rem 1.2rem;
        color:var(--black);
        text-transform: none;
    }

    .login-form-container form .checkbox{
        display:flex;
        align-items: center;
        gap:.5rem;
        padding:1rem 0;
    }

    .login-form-container form .checkbox label{
        font-size: 1.5rem;
        color:var(--light-color);
        cursor: pointer;
    }

    .login-form-container form .btn{
        text-align: center;
        width:100%;
        margin:1.5rem 0;
    }

    .login-form-container form p{
        padding-top: .8rem;
        color:var(--light-color);
        font-size: 1.5rem;
    }

    .login-form-container form p a{
        color:var(--green);
        text-decoration: underline;
    }

    .login-form-container #close-login-btn{
        position: absolute;
        top:1.5rem; right:2.5rem;
        font-size: 5rem;
        color:var(--black);
        cursor: pointer;
    }

    .home{
        background: url(../image/banner-bg.jpg) no-repeat;
        background-size: cover;
        background-position: center;
    }
    .banner1{
        background: url(banner/food.png) no-repeat;
        background-size: cover;
        background-position: center;
        width: 100%;
        height: 400px;
        /* font-size: 70px;
        line-height: 200px; */

    }
    .banner2{
        background: url(banner/dessert2.png);
        background-size: cover;
        background-position: center;
        width: 100%;
        height: 400px;
    }
    .food-menu{
        width: 100%;
        /* background-color: red; */
        background-color: #cbd1ce88;
        border: 2px solid gray;
    }
    .food{
        cursor: pointer;
        width: 30%;
        height: 260px;
        /* background-color: rgb(184, 181, 181); */
        margin-left: 2.5%;
        margin-top: 10px;
        margin-bottom: 10px;
        float: left;
        border: 1px solid gray;
    }
    .food-menu .food .img{
        width: 100%;
        height: 79%;
        margin-top: -23px;
        /* background-color: red; */
        float: left;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    /*.food-menu .food .text .price{*/
    /*    display:;*/
    /*}*/
  /*.food-menu .food .img img{*/
  /*      background-position: center;*/
  /*      background-repeat: no-repeat;*/
  /*      background-size: cover;*/
  /*      width: 100%;*/
  /*      height: 100%;*/
  /*  }*/

    .food-menu .food .txt{
        width: 100%;
        height: 21%;
        background-color: beige;
        float: left;
        text-align: center;
        font-size: 18px;
         line-height: 35px;

        display: flex;
        justify-content: space-around;
    }

    .food-menu .food .txt p{
        /*display: flex;*/
        /*justify-content: center;*/
        margin-top: 8px;
        display: inline-block;font-weight: bold
    }
    .food-menu .food .txt b{
        color: gray;
        font-weight: normal;
    }
    .home .row{
        display:flex;
        align-items: center;
        flex-wrap: wrap;
        gap:1.5rem;
    }

    .home .row .content{
        flex:1 1 42rem;
    }

    .home .row .books-slider{
        flex:1 1 42rem;
        text-align: center;
        margin-top: 2rem;
    }

    .home .row .books-slider a img{
        height: 25rem;
    }

    .home .row .books-slider a:hover img{
        transform: scale(.9);
    }

    .home .row .books-slider .stand{
        width:100%;
        margin-top: -2rem;
    }

    .home .row .content h3{
        color:var(--black);
        font-size: 25px;
    }

    .home .row .content p{
        color:var(--light-color);
        font-size: 16px;
        line-height: 2;
        padding:1rem 0;
    }

    .icons-container{
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(25rem, 1fr));
        gap:1.5rem;
    }

    .icons-container .icons{
        display: flex;
        align-items: center;
        gap:1.5rem;
        padding:2rem 0;
    }

    .icons-container .icons i{
        font-size: 3.5rem;
        color:var(--green);
    }

    .icons-container .icons h3{
        font-size: 2.2rem;
        color:var(--black);
        padding-bottom: .5rem;
    }

    .icons-container .icons p{
        font-size: 1.4rem;
        color:var(--light-color);
    }

    .featured .featured-slider .box{
        margin:2rem 0;
        position: relative;
        overflow: hidden;
        border:var(--border);
        text-align: center;
    }

    .featured .featured-slider .box:hover{
        border:var(--border-hover);
    }

    .featured .featured-slider .box .image{
        padding:1rem;
        background: linear-gradient(15deg, #eee 30%, #fff 30.1%);
    }

    .featured .featured-slider .box:hover .image{
        transform: translateY(6rem);
    }

    .featured .featured-slider .box .image img{
        height: 25rem;
    }

    .featured .featured-slider .box .icons{
        border-bottom: var(--border-hover);
        position: absolute;
        top:0; left:0; right: 0;
        background: #fff;
        z-index: 1;
        transform: translateY(-105%);
    }

    .featured .featured-slider .box:hover .icons{
        transform: translateY(0%);
    }

    .featured .featured-slider .box .icons a{
        color:var(--black);
        font-size: 2.2rem;
        padding:1.3rem 1.5rem;
    }

    .featured .featured-slider .box .icons a:hover{
        background:var(--green);
        color:#fff;
    }

    .featured .featured-slider .box .content{
        background:#eee;
        padding:1.5rem;
    }

    .featured .featured-slider .box .content h3{
        font-size: 2rem;
        color:var(--black);
    }

    .featured .featured-slider .box .content .price{
        font-size: 2.2rem;
        color:var(--black);
        padding-top: 1rem;
    }

    .featured .featured-slider .box .content .price span{
        font-size: 1.5rem;
        color:var(--light-color);
        text-decoration: line-through;
    }

    .swiper-button-next,
    .swiper-button-prev{
        border:var(--border-hover);
        height:4rem;
        width:4rem;
        color:var(--black);
        background: #fff;
    }

    .swiper-button-next::after,
    .swiper-button-prev::after{
        font-size: 2rem;
    }

    .swiper-button-next:hover,
    .swiper-button-prev:hover{
        background: var(--black);
        color:#fff;
    }

    .newsletter{
        background:url(../image/letter-bg.jpg) no-repeat;
        background-size: cover;
        background-position: center;
        background-attachment: fixed;
    }

    .newsletter form{
        max-width: 45rem;
        margin-left: auto;
        text-align: center;
        padding:5rem 0;
    }

    .newsletter form h3{
        font-size: 2.2rem;
        color:#fff;
        padding-bottom: .7rem;
        font-weight: normal;
    }

    .newsletter form .box{
        width: 100%;
        margin: .7rem 0;
        padding:1rem 1.2rem;
        font-size: 1.6rem;
        color:var(--black);
        border-radius: .5rem;
        text-transform: none;
    }

    .arrivals .arrivals-slider .box{
        display: flex;
        align-items:center;
        gap:1.5rem;
        padding:2rem 1rem;
        border:var(--border);
        margin:1rem 0;
    }

    .arrivals .arrivals-slider .box:hover{
        border:var(--border-hover);
    }

    .arrivals .arrivals-slider .box .image img{
        height:15rem;
    }

    .arrivals .arrivals-slider .box .content h3{
        font-size: 2rem;
        color:var(--black);
    }

    .arrivals .arrivals-slider .box .content .price{
        font-size: 2.2rem;
        color:var(--black);
        padding-bottom: .5rem;
    }

    .arrivals .arrivals-slider .box .content .price span{
        font-size: 1.5rem;
        color:var(--light-color);
        text-decoration: line-through;
    }

    .arrivals .arrivals-slider .box .content .stars i{
        font-size: 1.5rem;
        color:var(--green);
    }

    .deal{
        background:#f3f3f3;
        display: flex;
        align-items: center;
        flex-wrap: wrap;
        gap:1.5rem;
    }
    .deal a{
        color: rgb(255, 255, 255);
        font-size: 20px;
        margin-left: 40%;
        font-weight: 400;
    }
    .deal .content{
        flex:1 1 42rem;
    }

    .deal .image{
        flex:1 1 42rem;
    }

    .deal .image img{
        width: 100%;
    }

    .deal .content h3{
        color:var(--green);
        font-size: 2.5rem;
        padding-bottom: .5rem;
    }

    .deal .content h1{
        color:var(--black);
        font-size: 30px;
    }

    .deal .content p{
        padding:1rem 0;
        color:var(--light-color);
        font-size: 20px;
        line-height: 2;
    }

    .reviews .reviews-slider .box{
        border:var(--border);
        padding:2rem;
        text-align: center;
        margin:2rem 0;
    }

    .reviews .reviews-slider .box:hover{
        border:var(--border-hover);
    }

    .reviews .reviews-slider .box img{
        height:7rem;
        width:7rem;
        border-radius: 50%;
        object-fit: cover;
    }

    .reviews .reviews-slider .box h3{
        color:var(--black);
        font-size: 2.2rem;
        padding:.5rem 0;
    }

    .reviews .reviews-slider .box p{
        color:var(--light-color);
        font-size: 1.4rem;
        padding:1rem 0;
        line-height: 2;
    }

    .reviews .reviews-slider .box .stars{
        padding-top: .5rem;
    }

    .reviews .reviews-slider .box .stars i{
        font-size: 1.7rem;
        color:var(--green);
    }

    .blogs .blogs-slider .box{
        margin:2rem 0;
        border:var(--border);
    }

    .blogs .blogs-slider .box:hover{
        border:var(--border-hover);
    }

    .blogs .blogs-slider .box .image{
        height: 25rem;
        width: 100%;
        overflow: hidden;
    }

    .blogs .blogs-slider .box .image img{
        height: 100%;
        width: 100%;
        object-fit: cover;
    }

    .blogs .blogs-slider .box:hover .image img{
        transform: scale(1.1);
    }

    .blogs .blogs-slider .box .content{
        padding:1.5rem;
    }

    .blogs .blogs-slider .box .content h3{
        font-size: 2.2rem;
        color:var(--black);
    }

    .blogs .blogs-slider .box .content p{
        font-size: 1.4rem;
        color:var(--light-color);
        padding:1rem 0;
        line-height: 2;
    }
    .footer{
        border-top: 1px solid gray;
    }
    .footer .box-container{
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(25rem, 1fr));
        gap:1.5rem;
    }

    .footer .box-container .box h3{
        font-size: 2.2rem;
        color:var(--black);
        padding:1rem 0;
    }

    .footer .box-container .box a{
        display: block;
        font-size: 1.4rem;
        color:var(--light-color);
        padding:1rem 0;
    }

    .footer .box-container .box a i{
        color:var(--green);
        padding-right: .5rem;
    }

    .footer .box-container .box a:hover i{
        padding-right: 2rem;
    }

    .footer .box-container .box .map{
        width:100%;
    }
    .footer .box-container .box p{
        display: block;
        font-size: 1.4rem;
        color:var(--light-color);
        padding:1rem 0;
    }
    .footer .share{
        padding:1rem 0;
        text-align: center;
    }

    .footer .share a{
        height: 5rem;
        width: 5rem;
        line-height: 5rem;
        font-size: 2rem;
        color:#fff;
        background:var(--green);
        margin:0 .3rem;
        border-radius: 50%;
    }

    .footer .share a:hover{
        background:var(--black);
    }

    .footer .credit{
        border-top: var(--border);
        margin-top: 2rem;
        padding:0 1rem;
        padding-top: 2.5rem;
        font-size: 2rem;
        color:var(--light-color);
        text-align: center;
    }

    .footer .credit span{
        color:var(--green);
    }

    .loader-container{
        position: fixed;
        top:0; left: 0;
        height:100%;
        width: 100%;
        z-index: 10000;
        background:#f7f7f7;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .loader-container.active{
        display: none;
    }

    .loader-container img{
        height:10rem;
    }



    .content {
        padding-bottom: 114px;
        background-color: #fff;
    }
    .ctn__1 {
        padding-bottom: 44px;
    }
    .ic {
        border:0;
        float:right;
        background:#fff;
        color:#f00;
        width:50%;
        line-height:10px;
        font-size:10px;
        margin:-220% 0 0 0;
        overflow:hidden;
        padding:0
    }

    .container {
        margin-right: auto;
        margin-left: auto;
        *zoom: 1;

    }
    .container:before,
    .container:after {
        display: table;
        content: "";
        line-height: 0;
    }
    .container:after {
        clear: both;
    }
    .row {
        margin-left: -30px;
        *zoom: 1;
    }
    .row:before,
    .row:after {
        display: table;
        content: "";
        line-height: 0;
    }
    .row:after {
        clear: both;
    }
    .grid_12 {
        width: 1170px;
    }
    .map {
        overflow: hidden;
        padding-top: 3px;
        padding-bottom: 22px;
        /* background-color: red; */
    }


    .map figure {
        position: relative;
        display: block;
        width: 100%;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -o-box-sizing: border-box;
        box-sizing: border-box;
    }

    .map figure iframe {
        width: 100%;
        height: 445px;
        max-width: 100%;
    }
    .grid_12 {
        width: 1170px;
    }
    .grid_12 h2{
        margin: 0;
        padding: 0;
        border: 0;
        font-size: 100%;
        font: inherit;
        vertical-align: top;
        font-size: 36px;
        line-height: 48px;
        padding-top: 38px;
        margin-bottom: 14px;
        font-weight: bold;
        color: #535151;
        font-family: 'Economica', sans-serif;
    }
    .grid_3 {
        width: 270px;
    }
    [class*="grid_"] {
        float: left;
        min-height: 1px;
        margin-left: 30px;
    }
    .map_block {
        padding-top: 20px;
        color: #535151;
        font-size: 16px;
        line-height: 30px;
        font-weight: normal;
    }

    .map_block+.map_block {
        padding-top: 24px;
    }

    .map_block  .map_title {
        margin-bottom: 4px;
        font-weight: 600;
    }
    .grid_8 {
        width: 770px;
    }
    .color1 {
        color: #535151;
    }

    .grid_9{
        width: 870px;
    }


    .form_block {
        background-color: #f1f0f1;
        padding-bottom: 49px;
        border-bottom: 1px solid #fff;
        background-color: red;
    }
    .form_block .container{
        margin-right: auto;
        margin-left: auto;
        *zoom: 1;
        width: 1170px;
    }
    .form_block .container .row {
        margin-left: -30px;
        *zoom: 1;
    }
    .grid_11 {
        width: 1070px;
    }
    [class*="grid_"] {
        float: left;
        min-height: 1px;
        margin-left: 30px;
    }
    .grid_11 h2{

        font-size: 36px;
        line-height: 48px;
        padding-top: 38px;
        margin-bottom: 14px;
        font-weight: bold;
        color: #535151;
        font-family: 'Economica', sans-serif;

    }


    /* media queries  */

    @media (max-width:991px){

        html{
            font-size: 55%;
        }

        .header .header-1{
            padding:2rem;
        }

        section{
            padding:3rem 2rem;
        }

    }

    @media (max-width:768px){

        html{
            scroll-padding-top: 0;
        }

        body{
            padding-bottom: 6rem;
        }

        .header .header-2{
            display:none;
        }

        .bottom-navbar{
            display: block;
        }

        #search-btn{
            display: inline-block;
        }

        .header .header-1{
            box-shadow: var(--box-shadow);
            position: relative;
        }

        .header .header-1 .search-form{
            position:absolute;
            top:-115%; right:2rem;
            width: 90%;
            box-shadow: var(--box-shadow);
        }

        .header .header-1 .search-form.active{
            top:115%;
        }

        .home .row .content{
            text-align: center;
        }

        .home .row .content h3{
            font-size: 3.5rem;
        }

        .newsletter{
            background-position: right;
        }

        .newsletter form{
            margin-left:0;
            max-width: 100%;
        }

    }

    @media (max-width:450px){

        html{
            font-size: 50%;
        }

    }



    .bk{
        width: 100%;
        left: 0;
        height: 100%;
        display: block;
        position: fixed;
        z-index: 1;
        top: 0;
        background-color: rgb(20,19,19,0.5);
    }
    .view-card{
        width: 100%;
        height: 35%;
        top:65%;
        position: fixed;
        background-color: white;
        justify-content: center;
        display: flex;
        visibility: hidden;
        left: 0;
        z-index: 2;
        bottom: 0;
        /*margin: 20px;*/
    }

    .view-card-order-content{
        height: 90%;
        width: 30%;
        position: fixed;
        top: 10%;
        z-index: 2;
        right: 0;
        display: none;
        float: left;
        background-color: white;
    }
    .view-card-order-content .view-card-order{
        width: 100%;
        height: 75%;
        overflow-y: auto;
        float: left;
    }
    .view-card-order-content .check-payment{
        margin-top: 10px;
        width: 100%;
        height: 25%;
        float: left;
        padding-left: 5px;
        padding-right: 5px;
    }
    .check-payment .check-out-payment-list{
        width: 100%;
        display: flex;
    }
    .view-card-order-content .check-payment .check-out-payment-list .sub-total-txt{
        width: 50%;
    }
    .view-card-order-content .check-payment .check-out-payment-list .sub-total-txt p{
        font-size: 15px;
        color: gray;
    }
    .view-card-order-content .check-payment .check-out-payment-list .sub-total-txt p1{
        font-size: 13px;
        color: gray;
    }
    .view-card-order-content  .check-payment .check-out-payment-list .sub-total-price{
        width: 50%;
        display: flex;
        justify-content: right;
    }

</style>
<body>

<!-- header section starts  -->
@php

    $order_product_detail = \Illuminate\Support\Facades\DB::table('orders')
    ->select('orders.total_price','orders.total_qty','orders.product_id','order_details.payment_status','orders.id')
    ->join('order_details','order_details.order_id','=','orders.id')
    ->join('products','products.id','=','orders.product_id')
    ->where('order_details.payment_status','unpaid')
    ->get();
@endphp
<div class="content" style="width: 100%;box-sizing: border-box;left: 0;top: 0;overflow-y: auto">

</div>
<div class="view-card-order-content">
    <div class="view-card-order">
        <div>
            <div style="text-align: right;margin: 5px;cursor: pointer"><i class="fas fa-times cancel-x" style="font-size: 20px"></i></div>
            @foreach($order_product_detail as $order)
                @php
                    $list_product = \App\Models\Food\Product::find(optional($order)->product_id);
                @endphp
            <div style="display: flex;justify-content: space-between;margin: 10px" >
                <div>
                    <img style="width: 100px;height: 100px" src="{{url("/asset/".$list_product->price_img)}}" alt="">

                </div>
                <div>
                    <div style="margin-top: 5px;font-size: 12px;color: black">Name: {{$list_product->product_name}}</div>
                    <div style="margin-top: 5px;font-size: 12px;color: black">Price: {{$list_product->price}}</div>
                    <p style="margin-top: 5px;font-size: 12px;color: black">Quantity: </p>
                    <div style="display: flex;margin-top: 5px">
                        <div><button class="minus-cart-order-detail" type="button" style="padding: 5px;width: 30px;cursor: pointer">
                                -
                            </button></div>
                        <div><button class="btn-text-value"  value="" style="padding: 5px;width: 70px;margin-left: 3px;margin-right: 3px">{{$order->total_qty}}</button></div>
                        <div><button class="add-cart-order-detail" type="button" style="padding: 5px;width: 30px;cursor: pointer">+</button></div>
                    </div>

                </div>
                <div><i style="font-size: 20px" class="far fa-trash-alt"></i></div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="check-payment">
        <div class="check-out-payment-list">
            <div class="sub-total-txt">
                <p>Amount to Payment </p>
            </div>
            <div class="sub-total-price">
                <span>$40</span>
            </div>
        </div>
        <hr style="border: 1px solid gray">
        <div class="check-out-payment-list">
            <div class="sub-total-txt" style="margin-top: 3px">
                <p1>Total QTY </p1>
            </div>
            <div class="sub-total-price">
                <span>3</span>
            </div>
        </div>
        <div class="check-out-payment-list">
            <div class="sub-total-txt" style="margin-top: 3px">
                <p1>Total Price </p1>
            </div>
            <div class="sub-total-price">
                <span>$40</span>
            </div>
        </div>
        <div class="check-out-payment-list-shop" style="width: 100%;margin-top: 25px;display: flex;justify-content: center;align-items: center">

            <a style="width: 60%;justify-content: right;display: flex;cursor: pointer" href="{{url('/checkout')}}">
                <div class="sub-check-out" style="width: 100%;height: 100%;" >
                    <p style="width: 80%;background-color: black;height: 40px;border-radius: 4px;color: white;display: flex;justify-content: center;align-items: center">Check Out</p>
                </div>
            </a>
            <div class="sub-shop" style="width: 40%;margin-left: 6px;cursor: pointer;">
                <p style="width: 80%;background-color: black;height: 40px;border-radius: 4px;color: white;display: flex;justify-content: center;align-items: center">Shop Now</p>
            </div>
        </div>
    </div>
</div>
<div class="view-card">

</div>
<header class="header">

    <div class="header-1">

        <a href="#"><img src="{{url('asset/logo/3.png')}}" alt="" style="width:120px;height:120px;"></a>

        <!-- <a href="#" class="logo"> <i class="fas fa-book"></i> bookly </a> -->

        <form action="" class="search-form">
            <input type="search" name="" placeholder="search here..." id="search-box" class="input-search">
            <label for="search-box" class="fas fa-search search-box"></label>
        </form>

        <div class="icons">
            <div id="search-btn" class="fas fa-search"></div>
            <div class="fas fa-shopping-cart to-cart-order"></div>
            <a href="{{url('/login')}}"><div class="fas fa-user"></div></a>
        </div>

    </div>

    <div class="header-2">
        <nav class="navbar">
            <a href="{{url('/')}}">ទំព័រដើម</a>
            <a href="{{url('/food')}}">ម្ហូប</a>
            <a href="{{url('/dessert')}}">បង្អែម</a>
            <a href="{{url('/drink')}}">ភេសជ្ជះ</a>
{{--            <a href="{{url('/contact')}}">ទំនាក់ទំនង</a>--}}
            <a href="{{url('/order-history')}}">ប្រវត្តិនៃការកុម៉្មង់</a>
        </nav>
    </div>

</header>

<!-- header section ends -->

<!-- bottom navbar  -->

<nav class="bottom-navbar">
    <a href="http://127.0.0.1:5500/home.html" class="fas fa-home"></a>
    <a href="http://127.0.0.1:5500/food.html" class="fas fa-list"></a>
    <a href="http://127.0.0.1:5500/desert.html" class="fas fa-tags"></a>
    <a href="http://127.0.0.1:5500/drink.html" class="fas fa-comments"></a>
    <a href="http://127.0.0.1:5500/contact.html" class="fas fa-blog"></a>
</nav>

<!-- login form  -->



{{--<div class="login-form-container">--}}

{{--    <div id="close-login-btn" class="fas fa-times"></div>--}}

{{--    <form method="POST" action="{{ route('login') }}">--}}
{{--        @csrf--}}
{{--        <h3>sign in</h3>--}}
{{--        <span>username</span>--}}
{{--        <input type="email" name="" class="box" placeholder="enter your email" id="">--}}
{{--        <span>password</span>--}}
{{--        <input type="password" name="" class="box" placeholder="enter your password" id="">--}}
{{--        <div class="checkbox">--}}
{{--            <input type="checkbox" name="" id="remember-me">--}}
{{--            <label for="remember-me"> remember me</label>--}}
{{--        </div>--}}
{{--        <input type="submit" value="sign in" class="btn">--}}
{{--        <p>forget password ? <a href="#">click here</a></p>--}}
{{--        <p>don't have an account ? <a href="{{}}">create one</a></p>--}}
{{--    </form>--}}

{{--</div>--}}

<div>
    @yield('html')
</div>

<section class="footer">

    <div class="box-container">
        <div class="box">
            <!-- <img src="logo/2.png" alt="" width="200px" height="200px"> -->
            <h3>ទំនាក់ទំនង</h3>
            <a href="https://www.google.com/maps/place/Phnom+Penh/@11.5793306,104.7500961,11z/data=!3m1!4b1!4m5!3m4!1s0x3109513dc76a6be3:0x9c010ee85ab525bb!8m2!3d11.5563738!4d104.9282099"><i class="fas fa-map-marker-alt"></i>&nbsp; Phnom Penh</a>
            <a href="#"> <i class="fas fa-phone"></i> 0123456789 </a>
            <a href="#"> <i class="fas fa-phone"></i> 0987654321 </a>
            <a href="mailto: phearumrin34@gmail.com"> <i class="fas fa-envelope"></i> exam@gmail.com </a>

        </div>



        <div class="box">
            <h3>តំណភ្ជាប់រហ័ស</h3>
            <a href="http://127.0.0.1:5500/home.html"> <i class="fas fa-arrow-right"></i> ទំព័រដើម </a>
            <a href="http://127.0.0.1:5500/food.html"> <i class="fas fa-arrow-right"></i> ម្ហូប </a>
            <a href="http://127.0.0.1:5500/desert.html"> <i class="fas fa-arrow-right"></i> បង្អែម </a>
            <a href="http://127.0.0.1:5500/drink.html"> <i class="fas fa-arrow-right"></i> ភេសជ្ជះ </a>
            <a href="http://127.0.0.1:5500/contact.html"> <i class="fas fa-arrow-right"></i> ទំនាក់ទំនង</a>
        </div>




        <div class="box">
            <h3>ម៉ោងធ្វើការ</h3>
            <p>ច័ន្ទ
                <span style="float: right;">៩ព្រឹក​ ដល់​ ៦ល្ងាច</span>
            </p>
            <p>អង្គារ៏
                <span style="float: right;">៩ព្រឹក​ ដល់​ ៦ល្ងាច</span>
            </p>
            <p>ពុធ
                <span style="float: right;">៩ព្រឹក​ ដល់​ ៦ល្ងាច</span>
            </p>
            <p>ព្រហស្បត្តិ៏
                <span style="float: right;">៩ព្រឹក​ ដល់​ ៦ល្ងាច</span>
            </p>
            <p>សុក្រ
                <span style="float: right;">៩ព្រឹក​ ដល់​ ៦ល្ងាច</span>
            </p>
            <p>សៅរ៏
                <span style="float: right;">៩ព្រឹក​ ដល់​ ៦ល្ងាច</span>
            </p>

        </div>

    </div>

    <div class="share">
        <a href="#" class="fab fa-facebook-f"></a>
        <a href="#" class="fab fa-telegram"></a>
        <a href="mailto: phearumrin34@gmail.com" class="fas fa-envelope"></a>
    </div>

    <div class="credit"> created by <span>Team 5</span> | all rights reserved! </div>

</section>

<!-- footer section ends -->

<!-- loader  -->

<div class="loader-container">
    <img src="image/loading.gif" alt="">
</div>
















<script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>

<!-- custom js file link  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>


</body>


<script >
    searchForm = document.querySelector('.search-form');

    document.querySelector('#search-btn').onclick = () =>{
        searchForm.classList.toggle('active');
    }

    // let loginForm = document.querySelector('.login-form-container');

    // document.querySelector('#login-btn').onclick = () =>{
    //     loginForm.classList.toggle('active');
    // }

    // document.querySelector('#close-login-btn').onclick = () =>{
    //     loginForm.classList.remove('active');
    // }

    window.onscroll = () =>{

        searchForm.classList.remove('active');

        if(window.scrollY > 80){
            document.querySelector('.header .header-2').classList.add('active');
        }else{
            document.querySelector('.header .header-2').classList.remove('active');
        }

    }

    window.onload = () =>{

        if(window.scrollY > 80){
            document.querySelector('.header .header-2').classList.add('active');
        }else{
            document.querySelector('.header .header-2').classList.remove('active');
        }

        fadeOut();

    }

    function loader(){
        document.querySelector('.loader-container').classList.add('active');
    }

    function fadeOut(){
        setTimeout(loader, 4000);
    }

    var swiper = new Swiper(".books-slider", {
        loop:true,
        centeredSlides: true,
        autoplay: {
            delay: 9500,
            disableOnInteraction: false,
        },
        breakpoints: {
            0: {
                slidesPerView: 1,
            },
            768: {
                slidesPerView: 2,
            },
            1024: {
                slidesPerView: 3,
            },
        },
    });

    var swiper = new Swiper(".featured-slider", {
        spaceBetween: 10,
        loop:true,
        centeredSlides: true,
        autoplay: {
            delay: 9500,
            disableOnInteraction: false,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        breakpoints: {
            0: {
                slidesPerView: 1,
            },
            450: {
                slidesPerView: 2,
            },
            768: {
                slidesPerView: 3,
            },
            1024: {
                slidesPerView: 4,
            },
        },
    });

    var swiper = new Swiper(".arrivals-slider", {
        spaceBetween: 10,
        loop:true,
        centeredSlides: true,
        autoplay: {
            delay: 9500,
            disableOnInteraction: false,
        },
        breakpoints: {
            0: {
                slidesPerView: 1,
            },
            768: {
                slidesPerView: 2,
            },
            1024: {
                slidesPerView: 3,
            },
        },
    });

    var swiper = new Swiper(".reviews-slider", {
        spaceBetween: 10,
        grabCursor:true,
        loop:true,
        centeredSlides: true,
        autoplay: {
            delay: 9500,
            disableOnInteraction: false,
        },
        breakpoints: {
            0: {
                slidesPerView: 1,
            },
            768: {
                slidesPerView: 2,
            },
            1024: {
                slidesPerView: 3,
            },
        },
    });

    var swiper = new Swiper(".blogs-slider", {
        spaceBetween: 10,
        grabCursor:true,
        loop:true,
        centeredSlides: true,
        autoplay: {
            delay: 9500,
            disableOnInteraction: false,
        },
        breakpoints: {
            0: {
                slidesPerView: 1,
            },
            768: {
                slidesPerView: 2,
            },
            1024: {
                slidesPerView: 3,
            },
        },
    });


    $(document).ready(function (){


        //Search
        $('.search-box').click(function () {
            var input_text = $('.input-search').val();
            // if (input_text == ''){
            //     $('.search-category').remove();
            // }
            var url = 'find-category';
            $.ajax({
                type: 'GET',
                url: url,
                data: {
                    input_text: input_text,
                },
                success: function (data) {
                    alert(data);
                    // $('.content').append(data).html();
                }
            });
        })


        $('.to-cart-order').click(function () {
                var bk = '<div class="bk"></div>';
                $('body').append(bk);
                $('.view-card').css("visibility","hidden");
                $('.view-card-order-content').css("display","block")
        });

        // Cancel X
        $('.cancel-x').click(function (){

            $('.bk').remove();
            $('.view-card-order-content').css("display","none")
            $(".input-manulator").val("1");
            $(".btn-text-value").text("1");

        });

        //minus-cart-order-detail

        $('.minus-cart-order-detail').click(function (){
            var num = 1;
            var total_num =  $(".btn-text-value").text();
            var total_number = parseFloat(total_num)-parseFloat(num);
            if (total_number<1){
                total_number=1;
            }
            $(".btn-text-value").text(total_number);
        });

        //adds-cart-order-detail
        $('.add-cart-order-detail').click(function (){

            var num = 1;
            var total_num =  $(".btn-text-value").val();
            alert(total_num);
            var total_number = parseFloat(total_num)+parseFloat(num);
            $(".btn-text-value").text(total_number);
        });




    })
</script>


</html>
@else
    @include('user-admin.dashborad')
@endif
@else
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Khmer Food</title>
        <link rel="icon" href="logo/2.png">

        <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />

        <!-- font awesome cdn link  -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

        <!-- custom css file link  -->
        <link rel="stylesheet" href="style.css">

    </head>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Battambang:wght@100;300;400;500;600&display=swap');

        :root{
            --green:#27ae60;
            --dark-color:#219150;
            --black:#444;
            --light-color:#666;
            --border:.1rem solid rgba(0,0,0,.1);
            --border-hover:.1rem solid var(--black);
            --box-shadow:0 .5rem 1rem rgba(0,0,0,.1);
        }

        *{
            font-family: 'Poppins', sans-serif,'Battambang';
            /* font-family: Arial,'Battambang', cursive; */
            margin:0; padding:0;
            box-sizing: border-box;
            outline: none; border:none;
            text-decoration: none;
            text-transform: capitalize;
            transition:all .2s linear;
            transition:  none;
        }

        html{
            font-size: 62.5%;
            overflow-x: hidden;
            scroll-padding-top: 5rem;
            scroll-behavior: smooth;
        }

        html::-webkit-scrollbar{
            width:1rem;
        }

        html::-webkit-scrollbar-track{
            background:transparent;
        }

        html::-webkit-scrollbar-thumb{
            background:var(--black);
        }

        section{
            padding:5rem 9%;
        }

        .heading{
            text-align: center;
            margin-bottom: 2rem;
            position: relative;
        }

        .heading::before{
            content: '';
            position: absolute;
            top:50%; left:0;
            transform: translateY(-50%);
            width: 100%;
            height:.01rem;
            background: rgba(0,0,0,.1);
            z-index: -1;
        }

        .heading span{
            font-size: 3rem;
            padding:.5rem 2rem;
            color:var(--black);
            background: #fff;
            border:var(--border);
        }

        .btn{
            margin-top: 1rem;
            display:inline-block;
            padding:.9rem 3rem;
            border-radius: .5rem;
            color:#fff;
            background:var(--green);
            font-size: 1.7rem;
            cursor: pointer;
            font-weight: 500;
        }

        .btn:hover{
            background:var(--dark-color);
        }

        .header .header-1{
            background:#fff;
            padding:1.5rem 9%;
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .header .header-1 .logo{
            font-size: 2.5rem;
            font-weight: bolder;
            color:var(--black);
        }

        .header .header-1 .logo i{
            color:var(--green);
        }

        .header .header-1 .search-form{
            width:50rem;
            height:5rem;
            border:var(--border);
            overflow: hidden;
            background:#fff;
            display:flex;
            align-items: center;
            border-radius: .5rem;
        }

        .header .header-1 .search-form input{
            font-size: 1.6rem;
            padding:0 1.2rem;
            height:100%;
            width:100%;
            text-transform: none;
            color:var(--black);
        }

        .header .header-1 .search-form label{
            font-size: 2.5rem;
            padding-right: 1.5rem;
            color:var(--black);
            cursor: pointer;
        }

        .header .header-1 .search-form label:hover{
            color:var(--green);
        }

        .header .header-1 .icons div,
        .header .header-1 .icons a{
            font-size: 2.5rem;
            margin-left: 1.5rem;
            color:var(--black);
            cursor: pointer;
        }

        .header .header-1 .icons div:hover,
        .header .header-1 .icons a:hover{
            color:var(--green);
        }

        #search-btn{
            display: none;
        }

        .header .header-2{
            background:var(--green);
        }

        .header .header-2 .navbar{
            text-align: center;
        }

        .header .header-2 .navbar a{
            color:#fff;
            display: inline-block;
            padding:1.2rem;
            font-size: 1.7rem;
        }

        .header .header-2 .navbar a:hover{
            background:var(--dark-color);
        }

        .header .header-2.active{
            position:fixed;
            top:0; left:0; right:0;
            z-index: 1000;

        }

        .bottom-navbar{
            text-align: center;
            background:var(--green);
            position: fixed;
            bottom:0; left:0; right:0;
            z-index: 1000;
            display: none;
        }

        .bottom-navbar a{
            font-size: 2.5rem;
            padding:2rem;
            color:#fff;
        }

        .bottom-navbar a:hover{
            background:var(--dark-color);
        }

        .login-form-container{
            display: flex;
            align-items: center;
            justify-content: center;
            background:rgba(255,255,255,.9);
            position:fixed;
            top:0; right:-105%;
            z-index: 10000;
            height:100%;
            width:100%;
        }

        .login-form-container.active{
            right:0;
        }

        .login-form-container form{
            background:#fff;
            border:var(--border);
            width:40rem;
            padding:2rem;
            box-shadow: var(--box-shadow);
            border-radius: .5rem;
            margin:2rem;
        }

        .login-form-container form h3{
            font-size: 2.5rem;
            text-transform: uppercase;
            color:var(--black);
            text-align: center;
        }

        .login-form-container form span{
            display: block;
            font-size: 1.5rem;
            padding-top: 1rem;
        }

        .login-form-container form .box{
            width: 100%;
            margin:.7rem 0;
            font-size: 1.6rem;
            border:var(--border);
            border-radius: .5rem;
            padding:1rem 1.2rem;
            color:var(--black);
            text-transform: none;
        }

        .login-form-container form .checkbox{
            display:flex;
            align-items: center;
            gap:.5rem;
            padding:1rem 0;
        }

        .login-form-container form .checkbox label{
            font-size: 1.5rem;
            color:var(--light-color);
            cursor: pointer;
        }

        .login-form-container form .btn{
            text-align: center;
            width:100%;
            margin:1.5rem 0;
        }

        .login-form-container form p{
            padding-top: .8rem;
            color:var(--light-color);
            font-size: 1.5rem;
        }

        .login-form-container form p a{
            color:var(--green);
            text-decoration: underline;
        }

        .login-form-container #close-login-btn{
            position: absolute;
            top:1.5rem; right:2.5rem;
            font-size: 5rem;
            color:var(--black);
            cursor: pointer;
        }

        .home{
            background: url(../image/banner-bg.jpg) no-repeat;
            background-size: cover;
            background-position: center;
        }
        .banner1{
            background: url(banner/food.png) no-repeat;
            background-size: cover;
            background-position: center;
            width: 100%;
            height: 400px;
            /* font-size: 70px;
            line-height: 200px; */

        }
        .banner2{
            background: url(banner/dessert2.png);
            background-size: cover;
            background-position: center;
            width: 100%;
            height: 400px;
        }
        .food-menu{
            width: 100%;
            /* background-color: red; */
            background-color: #cbd1ce88;
            border: 2px solid gray;
        }
        .food{
            cursor: pointer;
            width: 30%;
            height: 260px;
            /* background-color: rgb(184, 181, 181); */
            margin-left: 2.5%;
            margin-top: 10px;
            margin-bottom: 10px;
            float: left;
            border: 1px solid gray;
        }
        .food-menu .food .img{
            width: 100%;
            height: 79%;
            margin-top: -23px;
            /* background-color: red; */
            float: left;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
        /*.food-menu .food .text .price{*/
        /*    display:;*/
        /*}*/
        /*.food-menu .food .img img{*/
        /*      background-position: center;*/
        /*      background-repeat: no-repeat;*/
        /*      background-size: cover;*/
        /*      width: 100%;*/
        /*      height: 100%;*/
        /*  }*/

        .food-menu .food .txt{
            width: 100%;
            height: 21%;
            background-color: beige;
            float: left;
            text-align: center;
            font-size: 18px;
            line-height: 35px;

            display: flex;
            justify-content: space-around;
        }

        .food-menu .food .txt p{
            /*display: flex;*/
            /*justify-content: center;*/
            margin-top: 8px;
            display: inline-block;font-weight: bold
        }
        .food-menu .food .txt b{
            color: gray;
            font-weight: normal;
        }
        .home .row{
            display:flex;
            align-items: center;
            flex-wrap: wrap;
            gap:1.5rem;
        }

        .home .row .content{
            flex:1 1 42rem;
        }

        .home .row .books-slider{
            flex:1 1 42rem;
            text-align: center;
            margin-top: 2rem;
        }

        .home .row .books-slider a img{
            height: 25rem;
        }

        .home .row .books-slider a:hover img{
            transform: scale(.9);
        }

        .home .row .books-slider .stand{
            width:100%;
            margin-top: -2rem;
        }

        .home .row .content h3{
            color:var(--black);
            font-size: 25px;
        }

        .home .row .content p{
            color:var(--light-color);
            font-size: 16px;
            line-height: 2;
            padding:1rem 0;
        }

        .icons-container{
            display: grid;
            grid-template-columns: repeat(auto-fit, minmax(25rem, 1fr));
            gap:1.5rem;
        }

        .icons-container .icons{
            display: flex;
            align-items: center;
            gap:1.5rem;
            padding:2rem 0;
        }

        .icons-container .icons i{
            font-size: 3.5rem;
            color:var(--green);
        }

        .icons-container .icons h3{
            font-size: 2.2rem;
            color:var(--black);
            padding-bottom: .5rem;
        }

        .icons-container .icons p{
            font-size: 1.4rem;
            color:var(--light-color);
        }

        .featured .featured-slider .box{
            margin:2rem 0;
            position: relative;
            overflow: hidden;
            border:var(--border);
            text-align: center;
        }

        .featured .featured-slider .box:hover{
            border:var(--border-hover);
        }

        .featured .featured-slider .box .image{
            padding:1rem;
            background: linear-gradient(15deg, #eee 30%, #fff 30.1%);
        }

        .featured .featured-slider .box:hover .image{
            transform: translateY(6rem);
        }

        .featured .featured-slider .box .image img{
            height: 25rem;
        }

        .featured .featured-slider .box .icons{
            border-bottom: var(--border-hover);
            position: absolute;
            top:0; left:0; right: 0;
            background: #fff;
            z-index: 1;
            transform: translateY(-105%);
        }

        .featured .featured-slider .box:hover .icons{
            transform: translateY(0%);
        }

        .featured .featured-slider .box .icons a{
            color:var(--black);
            font-size: 2.2rem;
            padding:1.3rem 1.5rem;
        }

        .featured .featured-slider .box .icons a:hover{
            background:var(--green);
            color:#fff;
        }

        .featured .featured-slider .box .content{
            background:#eee;
            padding:1.5rem;
        }

        .featured .featured-slider .box .content h3{
            font-size: 2rem;
            color:var(--black);
        }

        .featured .featured-slider .box .content .price{
            font-size: 2.2rem;
            color:var(--black);
            padding-top: 1rem;
        }

        .featured .featured-slider .box .content .price span{
            font-size: 1.5rem;
            color:var(--light-color);
            text-decoration: line-through;
        }

        .swiper-button-next,
        .swiper-button-prev{
            border:var(--border-hover);
            height:4rem;
            width:4rem;
            color:var(--black);
            background: #fff;
        }

        .swiper-button-next::after,
        .swiper-button-prev::after{
            font-size: 2rem;
        }

        .swiper-button-next:hover,
        .swiper-button-prev:hover{
            background: var(--black);
            color:#fff;
        }

        .newsletter{
            background:url(../image/letter-bg.jpg) no-repeat;
            background-size: cover;
            background-position: center;
            background-attachment: fixed;
        }

        .newsletter form{
            max-width: 45rem;
            margin-left: auto;
            text-align: center;
            padding:5rem 0;
        }

        .newsletter form h3{
            font-size: 2.2rem;
            color:#fff;
            padding-bottom: .7rem;
            font-weight: normal;
        }

        .newsletter form .box{
            width: 100%;
            margin: .7rem 0;
            padding:1rem 1.2rem;
            font-size: 1.6rem;
            color:var(--black);
            border-radius: .5rem;
            text-transform: none;
        }

        .arrivals .arrivals-slider .box{
            display: flex;
            align-items:center;
            gap:1.5rem;
            padding:2rem 1rem;
            border:var(--border);
            margin:1rem 0;
        }

        .arrivals .arrivals-slider .box:hover{
            border:var(--border-hover);
        }

        .arrivals .arrivals-slider .box .image img{
            height:15rem;
        }

        .arrivals .arrivals-slider .box .content h3{
            font-size: 2rem;
            color:var(--black);
        }

        .arrivals .arrivals-slider .box .content .price{
            font-size: 2.2rem;
            color:var(--black);
            padding-bottom: .5rem;
        }

        .arrivals .arrivals-slider .box .content .price span{
            font-size: 1.5rem;
            color:var(--light-color);
            text-decoration: line-through;
        }

        .arrivals .arrivals-slider .box .content .stars i{
            font-size: 1.5rem;
            color:var(--green);
        }

        .deal{
            background:#f3f3f3;
            display: flex;
            align-items: center;
            flex-wrap: wrap;
            gap:1.5rem;
        }
        .deal a{
            color: rgb(255, 255, 255);
            font-size: 20px;
            margin-left: 40%;
            font-weight: 400;
        }
        .deal .content{
            flex:1 1 42rem;
        }

        .deal .image{
            flex:1 1 42rem;
        }

        .deal .image img{
            width: 100%;
        }

        .deal .content h3{
            color:var(--green);
            font-size: 2.5rem;
            padding-bottom: .5rem;
        }

        .deal .content h1{
            color:var(--black);
            font-size: 30px;
        }

        .deal .content p{
            padding:1rem 0;
            color:var(--light-color);
            font-size: 20px;
            line-height: 2;
        }

        .reviews .reviews-slider .box{
            border:var(--border);
            padding:2rem;
            text-align: center;
            margin:2rem 0;
        }

        .reviews .reviews-slider .box:hover{
            border:var(--border-hover);
        }

        .reviews .reviews-slider .box img{
            height:7rem;
            width:7rem;
            border-radius: 50%;
            object-fit: cover;
        }

        .reviews .reviews-slider .box h3{
            color:var(--black);
            font-size: 2.2rem;
            padding:.5rem 0;
        }

        .reviews .reviews-slider .box p{
            color:var(--light-color);
            font-size: 1.4rem;
            padding:1rem 0;
            line-height: 2;
        }

        .reviews .reviews-slider .box .stars{
            padding-top: .5rem;
        }

        .reviews .reviews-slider .box .stars i{
            font-size: 1.7rem;
            color:var(--green);
        }

        .blogs .blogs-slider .box{
            margin:2rem 0;
            border:var(--border);
        }

        .blogs .blogs-slider .box:hover{
            border:var(--border-hover);
        }

        .blogs .blogs-slider .box .image{
            height: 25rem;
            width: 100%;
            overflow: hidden;
        }

        .blogs .blogs-slider .box .image img{
            height: 100%;
            width: 100%;
            object-fit: cover;
        }

        .blogs .blogs-slider .box:hover .image img{
            transform: scale(1.1);
        }

        .blogs .blogs-slider .box .content{
            padding:1.5rem;
        }

        .blogs .blogs-slider .box .content h3{
            font-size: 2.2rem;
            color:var(--black);
        }

        .blogs .blogs-slider .box .content p{
            font-size: 1.4rem;
            color:var(--light-color);
            padding:1rem 0;
            line-height: 2;
        }
        .footer{
            border-top: 1px solid gray;
        }
        .footer .box-container{
            display: grid;
            grid-template-columns: repeat(auto-fit, minmax(25rem, 1fr));
            gap:1.5rem;
        }

        .footer .box-container .box h3{
            font-size: 2.2rem;
            color:var(--black);
            padding:1rem 0;
        }

        .footer .box-container .box a{
            display: block;
            font-size: 1.4rem;
            color:var(--light-color);
            padding:1rem 0;
        }

        .footer .box-container .box a i{
            color:var(--green);
            padding-right: .5rem;
        }

        .footer .box-container .box a:hover i{
            padding-right: 2rem;
        }

        .footer .box-container .box .map{
            width:100%;
        }
        .footer .box-container .box p{
            display: block;
            font-size: 1.4rem;
            color:var(--light-color);
            padding:1rem 0;
        }
        .footer .share{
            padding:1rem 0;
            text-align: center;
        }

        .footer .share a{
            height: 5rem;
            width: 5rem;
            line-height: 5rem;
            font-size: 2rem;
            color:#fff;
            background:var(--green);
            margin:0 .3rem;
            border-radius: 50%;
        }

        .footer .share a:hover{
            background:var(--black);
        }

        .footer .credit{
            border-top: var(--border);
            margin-top: 2rem;
            padding:0 1rem;
            padding-top: 2.5rem;
            font-size: 2rem;
            color:var(--light-color);
            text-align: center;
        }

        .footer .credit span{
            color:var(--green);
        }

        .loader-container{
            position: fixed;
            top:0; left: 0;
            height:100%;
            width: 100%;
            z-index: 10000;
            background:#f7f7f7;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .loader-container.active{
            display: none;
        }

        .loader-container img{
            height:10rem;
        }



        .content {
            padding-bottom: 114px;
            background-color: #fff;
        }
        .ctn__1 {
            padding-bottom: 44px;
        }
        .ic {
            border:0;
            float:right;
            background:#fff;
            color:#f00;
            width:50%;
            line-height:10px;
            font-size:10px;
            margin:-220% 0 0 0;
            overflow:hidden;
            padding:0
        }

        .container {
            margin-right: auto;
            margin-left: auto;
            *zoom: 1;

        }
        .container:before,
        .container:after {
            display: table;
            content: "";
            line-height: 0;
        }
        .container:after {
            clear: both;
        }
        .row {
            margin-left: -30px;
            *zoom: 1;
        }
        .row:before,
        .row:after {
            display: table;
            content: "";
            line-height: 0;
        }
        .row:after {
            clear: both;
        }
        .grid_12 {
            width: 1170px;
        }
        .map {
            overflow: hidden;
            padding-top: 3px;
            padding-bottom: 22px;
            /* background-color: red; */
        }


        .map figure {
            position: relative;
            display: block;
            width: 100%;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -o-box-sizing: border-box;
            box-sizing: border-box;
        }

        .map figure iframe {
            width: 100%;
            height: 445px;
            max-width: 100%;
        }
        .grid_12 {
            width: 1170px;
        }
        .grid_12 h2{
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: top;
            font-size: 36px;
            line-height: 48px;
            padding-top: 38px;
            margin-bottom: 14px;
            font-weight: bold;
            color: #535151;
            font-family: 'Economica', sans-serif;
        }
        .grid_3 {
            width: 270px;
        }
        [class*="grid_"] {
            float: left;
            min-height: 1px;
            margin-left: 30px;
        }
        .map_block {
            padding-top: 20px;
            color: #535151;
            font-size: 16px;
            line-height: 30px;
            font-weight: normal;
        }

        .map_block+.map_block {
            padding-top: 24px;
        }

        .map_block  .map_title {
            margin-bottom: 4px;
            font-weight: 600;
        }
        .grid_8 {
            width: 770px;
        }
        .color1 {
            color: #535151;
        }

        .grid_9{
            width: 870px;
        }


        .form_block {
            background-color: #f1f0f1;
            padding-bottom: 49px;
            border-bottom: 1px solid #fff;
            background-color: red;
        }
        .form_block .container{
            margin-right: auto;
            margin-left: auto;
            *zoom: 1;
            width: 1170px;
        }
        .form_block .container .row {
            margin-left: -30px;
            *zoom: 1;
        }
        .grid_11 {
            width: 1070px;
        }
        [class*="grid_"] {
            float: left;
            min-height: 1px;
            margin-left: 30px;
        }
        .grid_11 h2{

            font-size: 36px;
            line-height: 48px;
            padding-top: 38px;
            margin-bottom: 14px;
            font-weight: bold;
            color: #535151;
            font-family: 'Economica', sans-serif;

        }


        /* media queries  */

        @media (max-width:991px){

            html{
                font-size: 55%;
            }

            .header .header-1{
                padding:2rem;
            }

            section{
                padding:3rem 2rem;
            }

        }

        @media (max-width:768px){

            html{
                scroll-padding-top: 0;
            }

            body{
                padding-bottom: 6rem;
            }

            .header .header-2{
                display:none;
            }

            .bottom-navbar{
                display: block;
            }

            #search-btn{
                display: inline-block;
            }

            .header .header-1{
                box-shadow: var(--box-shadow);
                position: relative;
            }

            .header .header-1 .search-form{
                position:absolute;
                top:-115%; right:2rem;
                width: 90%;
                box-shadow: var(--box-shadow);
            }

            .header .header-1 .search-form.active{
                top:115%;
            }

            .home .row .content{
                text-align: center;
            }

            .home .row .content h3{
                font-size: 3.5rem;
            }

            .newsletter{
                background-position: right;
            }

            .newsletter form{
                margin-left:0;
                max-width: 100%;
            }

        }

        @media (max-width:450px){

            html{
                font-size: 50%;
            }

        }



        .bk{
            width: 100%;
            left: 0;
            height: 100%;
            display: block;
            position: fixed;
            z-index: 1;
            top: 0;
            background-color: rgb(20,19,19,0.5);
        }
        .view-card{
            width: 100%;
            height: 35%;
            top:65%;
            position: fixed;
            background-color: white;
            justify-content: center;
            display: flex;
            visibility: hidden;
            left: 0;
            z-index: 2;
            bottom: 0;
            /*margin: 20px;*/
        }

        .view-card-order-content{
            height: 90%;
            width: 30%;
            position: fixed;
            top: 10%;
            z-index: 2;
            right: 0;
            display: none;
            float: left;
            background-color: white;
        }
        .view-card-order-content .view-card-order{
            width: 100%;
            height: 75%;
            overflow-y: auto;
            float: left;
        }
        .view-card-order-content .check-payment{
            margin-top: 10px;
            width: 100%;
            height: 25%;
            float: left;
            padding-left: 5px;
            padding-right: 5px;
        }
        .check-payment .check-out-payment-list{
            width: 100%;
            display: flex;
        }
        .view-card-order-content .check-payment .check-out-payment-list .sub-total-txt{
            width: 50%;
        }
        .view-card-order-content .check-payment .check-out-payment-list .sub-total-txt p{
            font-size: 15px;
            color: gray;
        }
        .view-card-order-content .check-payment .check-out-payment-list .sub-total-txt p1{
            font-size: 13px;
            color: gray;
        }
        .view-card-order-content  .check-payment .check-out-payment-list .sub-total-price{
            width: 50%;
            display: flex;
            justify-content: right;
        }

    </style>
    <body>
    <div class="content" style="width: 100%;left: 0;top: 0;box-sizing: border-box;overflow-y: auto">

    </div>

    <!-- header section starts  -->

    <div class="view-card-order-content">
        <div class="view-card-order">
            <div>
                <div style="text-align: right;margin: 5px;cursor: pointer"><i class="fas fa-times cancel-x" style="font-size: 20px"></i></div>
                <div style="display: flex;justify-content: space-between;margin: 10px" >
                    <div>
                        <img style="width: 100px;height: 100px" src="{{url("/asset/Amok.jpg")}}" alt="">

                    </div>
                    <div>
                        <div style="margin-top: 5px;font-size: 12px;color: black">Name: ahjruk</div>
                        <div style="margin-top: 5px;font-size: 12px;color: black">Price: 12</div>
                        <p style="margin-top: 5px;font-size: 12px;color: black">Quantity:</p>
                        <div style="display: flex;margin-top: 5px">
                            <div><button class="minus-cart-order-detail" type="button" style="padding: 5px;width: 30px;cursor: pointer">
                                    -
                                </button></div>
                            <div><button class="btn-text-value"  value="1" style="padding: 5px;width: 70px;margin-left: 3px;margin-right: 3px">1</button></div>
                            <div><button class="add-cart-order-detail" type="button" style="padding: 5px;width: 30px;cursor: pointer">+</button></div>
                        </div>

                    </div>
                    <div><i style="font-size: 20px" class="far fa-trash-alt"></i></div>
                </div>
            </div>
        </div>
        <div class="check-payment">
            <div class="check-out-payment-list">
                <div class="sub-total-txt">
                    <p>Amount to Payment </p>
                </div>
                <div class="sub-total-price">
                    <span>$40</span>
                </div>
            </div>
            <hr style="border: 1px solid gray">
            <div class="check-out-payment-list">
                <div class="sub-total-txt" style="margin-top: 3px">
                    <p1>Total QTY </p1>
                </div>
                <div class="sub-total-price">
                    <span>3</span>
                </div>
            </div>
            <div class="check-out-payment-list">
                <div class="sub-total-txt" style="margin-top: 3px">
                    <p1>Total Price </p1>
                </div>
                <div class="sub-total-price">
                    <span>$40</span>
                </div>
            </div>
            <div class="check-out-payment-list-shop" style="width: 100%;margin-top: 25px;display: flex;justify-content: center;align-items: center">

                <a style="width: 60%;justify-content: right;display: flex;cursor: pointer" href="{{url('/checkout')}}">
                    <div class="sub-check-out" style="width: 100%;height: 100%;" >
                        <p style="width: 80%;background-color: black;height: 40px;border-radius: 4px;color: white;display: flex;justify-content: center;align-items: center">Check Out</p>
                    </div>
                </a>
                <div class="sub-shop" style="width: 40%;margin-left: 6px;cursor: pointer;">
                    <p style="width: 80%;background-color: black;height: 40px;border-radius: 4px;color: white;display: flex;justify-content: center;align-items: center">Shop Now</p>
                </div>
            </div>
        </div>
    </div>
    <div class="view-card">

    </div>
    <header class="header">

        <div class="header-1">

            <a href="#"><img src="{{url('asset/logo/3.png')}}" alt="" style="width:120px;height:120px;"></a>

            <!-- <a href="#" class="logo"> <i class="fas fa-book"></i> bookly </a> -->

            <form action="" class="search-form">
                <input type="search" name="" placeholder="search here..." id="search-box">
                <label for="search-box" class="fas fa-search"></label>
            </form>

            <div class="icons">
                <div id="search-btn" class="fas fa-search"></div>
{{--                <div class="fas fa-shopping-cart to-cart-order"></div>--}}
                <a href="{{url('/login')}}"><div class="fas fa-user"></div></a>
            </div>

        </div>

        <div class="header-2">
            <nav class="navbar">
                <a href="{{url('/')}}">ទំព័រដើម</a>
                <a href="{{url('/food')}}">ម្ហូប</a>
                <a href="{{url('/dessert')}}">បង្អែម</a>
                <a href="{{url('/drink')}}">ភេសជ្ជះ</a>
                <a href="{{url('/contact')}}">ទំនាក់ទំនង</a>
            </nav>
        </div>

    </header>

    <!-- header section ends -->

    <!-- bottom navbar  -->

    <nav class="bottom-navbar">
        <a href="http://127.0.0.1:5500/home.html" class="fas fa-home"></a>
        <a href="http://127.0.0.1:5500/food.html" class="fas fa-list"></a>
        <a href="http://127.0.0.1:5500/desert.html" class="fas fa-tags"></a>
        <a href="http://127.0.0.1:5500/drink.html" class="fas fa-comments"></a>
        <a href="http://127.0.0.1:5500/contact.html" class="fas fa-blog"></a>
    </nav>

    <!-- login form  -->



{{--    <div class="login-form-container">--}}

{{--        <div id="close-login-btn" class="fas fa-times"></div>--}}

{{--        <form method="POST" action="{{ route('login') }}">--}}
{{--            @csrf--}}
{{--            <h3>sign in</h3>--}}
{{--            <span>username</span>--}}
{{--            <input type="email" name="" class="box" placeholder="enter your email" id="">--}}
{{--            <span>password</span>--}}
{{--            <input type="password" name="" class="box" placeholder="enter your password" id="">--}}
{{--            <div class="checkbox">--}}
{{--                <input type="checkbox" name="" id="remember-me">--}}
{{--                <label for="remember-me"> remember me</label>--}}
{{--            </div>--}}
{{--            <input type="submit" value="sign in" class="btn">--}}
{{--            <p>forget password ? <a href="#">click here</a></p>--}}
{{--            <p>don't have an account ? <a href="{{}}">create one</a></p>--}}
{{--        </form>--}}

{{--    </div>--}}

    <div>
        @yield('html')
    </div>

    <section class="footer">

        <div class="box-container">
            <div class="box">
                <!-- <img src="logo/2.png" alt="" width="200px" height="200px"> -->
                <h3>ទំនាក់ទំនង</h3>
                <a href="https://www.google.com/maps/place/Phnom+Penh/@11.5793306,104.7500961,11z/data=!3m1!4b1!4m5!3m4!1s0x3109513dc76a6be3:0x9c010ee85ab525bb!8m2!3d11.5563738!4d104.9282099"><i class="fas fa-map-marker-alt"></i>&nbsp; Phnom Penh</a>
                <a href="#"> <i class="fas fa-phone"></i> 0123456789 </a>
                <a href="#"> <i class="fas fa-phone"></i> 0987654321 </a>
                <a href="mailto: phearumrin34@gmail.com"> <i class="fas fa-envelope"></i> exam@gmail.com </a>

            </div>

            <div class="box">
                <h3>តំណភ្ជាប់រហ័ស</h3>
                <a href="http://127.0.0.1:5500/home.html"> <i class="fas fa-arrow-right"></i> ទំព័រដើម </a>
                <a href="http://127.0.0.1:5500/food.html"> <i class="fas fa-arrow-right"></i> ម្ហូប </a>
                <a href="http://127.0.0.1:5500/desert.html"> <i class="fas fa-arrow-right"></i> បង្អែម </a>
                <a href="http://127.0.0.1:5500/drink.html"> <i class="fas fa-arrow-right"></i> ភេសជ្ជះ </a>
                <a href="http://127.0.0.1:5500/contact.html"> <i class="fas fa-arrow-right"></i> ទំនាក់ទំនង</a>
            </div>




            <div class="box">
                <h3>ម៉ោងធ្វើការ</h3>
                <p>ច័ន្ទ
                    <span style="float: right;">៩ព្រឹក​ ដល់​ ៦ល្ងាច</span>
                </p>
                <p>អង្គារ៏
                    <span style="float: right;">៩ព្រឹក​ ដល់​ ៦ល្ងាច</span>
                </p>
                <p>ពុធ
                    <span style="float: right;">៩ព្រឹក​ ដល់​ ៦ល្ងាច</span>
                </p>
                <p>ព្រហស្បត្តិ៏
                    <span style="float: right;">៩ព្រឹក​ ដល់​ ៦ល្ងាច</span>
                </p>
                <p>សុក្រ
                    <span style="float: right;">៩ព្រឹក​ ដល់​ ៦ល្ងាច</span>
                </p>
                <p>សៅរ៏
                    <span style="float: right;">៩ព្រឹក​ ដល់​ ៦ល្ងាច</span>
                </p>

            </div>

        </div>

        <div class="share">
            <a href="#" class="fab fa-facebook-f"></a>
            <a href="#" class="fab fa-telegram"></a>
            <a href="mailto: phearumrin34@gmail.com" class="fas fa-envelope"></a>
        </div>

        <div class="credit"> created by <span>Team 5</span> | all rights reserved! </div>

    </section>

    <!-- footer section ends -->

    <!-- loader  -->

    <div class="loader-container">
        <img src="image/loading.gif" alt="">
    </div>
















    <script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>

    <!-- custom js file link  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>


    </body>


    <script >
        searchForm = document.querySelector('.search-form');

        document.querySelector('#search-btn').onclick = () =>{
            searchForm.classList.toggle('active');
        }

        // let loginForm = document.querySelector('.login-form-container');

        // document.querySelector('#login-btn').onclick = () =>{
        //     loginForm.classList.toggle('active');
        // }

        // document.querySelector('#close-login-btn').onclick = () =>{
        //     loginForm.classList.remove('active');
        // }

        window.onscroll = () =>{

            searchForm.classList.remove('active');

            if(window.scrollY > 80){
                document.querySelector('.header .header-2').classList.add('active');
            }else{
                document.querySelector('.header .header-2').classList.remove('active');
            }

        }

        window.onload = () =>{

            if(window.scrollY > 80){
                document.querySelector('.header .header-2').classList.add('active');
            }else{
                document.querySelector('.header .header-2').classList.remove('active');
            }

            fadeOut();

        }

        function loader(){
            document.querySelector('.loader-container').classList.add('active');
        }

        function fadeOut(){
            setTimeout(loader, 4000);
        }

        var swiper = new Swiper(".books-slider", {
            loop:true,
            centeredSlides: true,
            autoplay: {
                delay: 9500,
                disableOnInteraction: false,
            },
            breakpoints: {
                0: {
                    slidesPerView: 1,
                },
                768: {
                    slidesPerView: 2,
                },
                1024: {
                    slidesPerView: 3,
                },
            },
        });

        var swiper = new Swiper(".featured-slider", {
            spaceBetween: 10,
            loop:true,
            centeredSlides: true,
            autoplay: {
                delay: 9500,
                disableOnInteraction: false,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            breakpoints: {
                0: {
                    slidesPerView: 1,
                },
                450: {
                    slidesPerView: 2,
                },
                768: {
                    slidesPerView: 3,
                },
                1024: {
                    slidesPerView: 4,
                },
            },
        });

        var swiper = new Swiper(".arrivals-slider", {
            spaceBetween: 10,
            loop:true,
            centeredSlides: true,
            autoplay: {
                delay: 9500,
                disableOnInteraction: false,
            },
            breakpoints: {
                0: {
                    slidesPerView: 1,
                },
                768: {
                    slidesPerView: 2,
                },
                1024: {
                    slidesPerView: 3,
                },
            },
        });

        var swiper = new Swiper(".reviews-slider", {
            spaceBetween: 10,
            grabCursor:true,
            loop:true,
            centeredSlides: true,
            autoplay: {
                delay: 9500,
                disableOnInteraction: false,
            },
            breakpoints: {
                0: {
                    slidesPerView: 1,
                },
                768: {
                    slidesPerView: 2,
                },
                1024: {
                    slidesPerView: 3,
                },
            },
        });

        var swiper = new Swiper(".blogs-slider", {
            spaceBetween: 10,
            grabCursor:true,
            loop:true,
            centeredSlides: true,
            autoplay: {
                delay: 9500,
                disableOnInteraction: false,
            },
            breakpoints: {
                0: {
                    slidesPerView: 1,
                },
                768: {
                    slidesPerView: 2,
                },
                1024: {
                    slidesPerView: 3,
                },
            },
        });


        $(document).ready(function (){
            $('.to-cart-order').click(function () {
                var bk = '<div class="bk"></div>';
                $('body').append(bk);
                $('.view-card').css("visibility","hidden");
                $('.view-card-order-content').css("display","block")
            });

            // Cancel X
            $('.cancel-x').click(function (){

                $('.bk').remove();
                $('.view-card-order-content').css("display","none")
                $(".input-manulator").val("1");
                $(".btn-text-value").text("1");

            });

            //minus-cart-order-detail

            $('.minus-cart-order-detail').click(function (){
                var num = 1;
                var total_num =  $(".btn-text-value").text();
                var total_number = parseFloat(total_num)-parseFloat(num);
                if (total_number<1){
                    total_number=1;
                }
                $(".btn-text-value").text(total_number);
            });

            //adds-cart-order-detail
            // $('.add-cart-order-detail').click(function (){
            //
            //     var num = 1;
            //     var total_num =  $(".btn-text-value").text();
            //     var total_number = parseFloat(total_num)+parseFloat(num);
            //     $(".btn-text-value").text(total_number);
            // });
        })
    </script>


    </html>
@endif
