@extends('layout.layout')
@section('html')
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Food Order Card UI Design</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
{{--    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">--}}
{{--    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">--}}
{{--    <link rel="stylesheet" type="text/css" href="css/style.css">--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css">
</head>
<style>
    @import {{url("https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap")}};
    body {
        background: #f9f9f9;
        font-family: "roboto", sans-serif;
    }
    .main-content {
        padding-top: 100px;
        padding-bottom: 100px;
    }

    .shadow {
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.06) !important;
    }

    .food-card {
        transition: 0.1s;
    }
    .food-card:hover {
        box-shadow: 0 5px 20px rgba(0, 0, 0, 0.1);
    }
    .food-card .food-card_img img {
        width: 100%;
        height: 200px;
        object-fit: cover;
    }
    .food-card .food-card_img i {
        position: absolute;
        top: 20px;
        right: 30px;
        color: #fff;
        font-size: 25px;
        transition: all 0.1s;
    }
    .food-card .food-card_img i:hover {
        top: 18px;
        right: 28px;
        font-size: 29px;
    }
    .food-card .food-card_content {
        padding: 15px;
    }
    .food-card .food-card_content .food-card_title-section {
        height: 100px;
    }
    .food-card .food-card_content .food-card_title-section .food-card_title {
        margin-bottom: 8px;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .food-card .food-card_content .food-card_title-section .food-card_author {
        font-size: 15px;
        display: block;
    }
    .food-card .food-card_content .food-card_bottom-section .food-card_price {
        font-size: 28px;
        font-weight: 500;
        color: #f47a00;
    }
    .food-card .food-card_content .food-card_bottom-section .food-card_order-count {
        width: 100%;
    }
    .food-card
    .food-card_content
    .food-card_bottom-section
    .food-card_order-count
    input {
        background: #f5f5f5;
        border-color: #f5f5f5;
        box-shadow: none;
        text-align: center;
    }
    button {
        border-color: #f5f5f5;
        border-width: 3px;
        box-shadow: none;
    }

    .rating-box {
        display: flex;
        align-items: center;
    }

    .rating-stars {
        width: 70px;
    }
    .rating-stars:before {
        content: "";
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: #eee;
    }
    .rating-stars .filled-star {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: #f47a00;
    }
    .rating-stars img {
        height: 100%;
        width: 100%;
        display: block;
        position: relative;
        z-index: 1;
    }

    @media (min-width: 992px) {
        .food-card--vertical {
            display: flex;
            position: relative;
            height: 235px;
        }
        .food-card--vertical .food-card_img img {
            width: 200px;
            height: 100%;
            object-fit: cover;
        }
    }

</style>
<body>
<section class="main-content">
{{--    <div class="bk"></div>--}}
    <div class="container">

        <h1  style="text-align: center;font-size: 30px">Product Detail</h1>
        <br>
        <br>
        <div class="row" style="display: flex;justify-content: center">

            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="food-card food-card--vertical bg-white rounded-lg overflow-hidden mb-4 shadow">
                    @foreach($product as $pro)
                        <div class="food-card_img position-relative">
                            <img src="{{url("/asset/".$pro->price_img)}}" alt="">
                        </div>
                        <div class="food-card_content">
                            <div class="food-card_title-section overflow-hidden">
                                <h1 class="food-card_title"><a href="#!" class="text-dark">Name :{{$pro->product_name}}</a></h1>
                                <div class="d-flex justify-content-between">
                                    <a href="#!" class="food-card_author">In stock : {{$pro->unit_in_stock}}</a>

                                </div>
                            </div>
                            <div class="food-card_bottom-section">

                                <hr>
                                <div class="">
                                    <div class="food-card_price">
                                        <input class="product_id" type="hidden" value="{{$pro->id}}">
                                        <span >Price : {{$pro->price}}$</span>
                                        <input type="hidden" class="price" value="{{$pro->price}}">
                                    </div>
                                    <div class="food-card_order-count">
                                        <div class="input-group mb-3">
                                            {{--                                        <div class="input-group-prepend ok">--}}
                                            {{--                                            <button class="btn btn-outline-secondary minus-btn" type="button" id="button-addon1"><i class="fa fa-minus"></i></button>--}}
                                            {{--                                        </div>--}}
                                            {{--                                        <input readonly type="text" class="form-control input-manulator" placeholder="" aria-label="Example text with button addon"--}}
                                            {{--                                               aria-describedby="button-addon1" value="0" style="margin-top: 12px">--}}
                                            {{--                                        <div class="input-group-append">--}}
                                            {{--                                            <button class="btn btn-outline-secondary add-btn" type="button" id="button-addon1"><i class="fa fa-plus"></i></button>--}}
                                            {{--                                        </div>--}}
                                            {{--                                        <div style="margin-left: 30px;width: 70px">--}}
                                            {{--                                            <button type="button" class="" style="border-radius: 15px">Add Food</button>--}}
                                            {{--                                        </div>--}}
                                            <div style="display: flex;justify-content: center;margin-top: 5px">
                                                <div><button class="minus-btn" type="button" style="padding: 8px;width: 30px;cursor: pointer">
                                                        <i class="fa fa-minus"></i>
                                                    </button></div>
                                                <input readonly class="input-manulator" value="1" type="text" name="" id="">
                                                <div><button class="add-btn" type="button" style="padding: 8px;width: 30px;cursor: pointer"><i class="fa fa-plus"></i></button></div>
                                            </div>
                                            @if(\Illuminate\Support\Facades\Auth::user())
                                                <div style="margin-top: 15px;display: flex;justify-content: center">
                                                    <button class="add-to-card" type="button" style="padding: 8px;border-radius: 5px;background-color: #27ae60;color: white;font-weight: bold;cursor: pointer">Add Food</button>
                                                </div>
                                            @else
                                                <a href="{{url('/login')}}"><div style="margin-top: 15px;display: flex;justify-content: center">
                                                        <button class="add-to-card" type="button" style="padding: 8px;border-radius: 5px;background-color: #27ae60;color: white;font-weight: bold;cursor: pointer">Add Food</button>
                                                    </div>
                                                </a>
                                            @endif

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

<script>
    $(document).ready(function (){
        $(".minus-btn ").click(function (){
            var num = 1;
            var total_num = $(".input-manulator").val();
            var total_number = parseFloat(total_num)-parseFloat(num);
            if (total_number<1){
                total_number=1;
            }
            $(".input-manulator").val(total_number);
        });
        $(".add-btn").click(function (){

            var num = 1;
            var total_num = $(".input-manulator").val();
            var total_number = parseFloat(total_num)+parseFloat(num);
            $(".input-manulator").val(total_number);
        });

        //Add To Cart

        $('.add-to-card').click(function (){
            var bk = '<div class="bk"></div>';
            var payment_status = "unpaid";
            var product_id = $('.product_id').val();
            var total_qty =  $(".input-manulator").val();
            var price =  $(".price").val();
            var total_price = parseFloat(total_qty)*parseFloat(price);



            var url = 'find-product-detail'+'/'+product_id;
            $.ajax({
                type:'GET',
                url:url,
                data:{
                    total_qty : total_qty,
                    product_id : product_id,
                    total_price : total_price,
                    payment_status : payment_status
                },
                success:function(data){
                    $('body').append(bk);
                    $('.view-card').css("visibility","visible").append(data);
                }
            });
        });







        });
</script>
</html>
@endsection
