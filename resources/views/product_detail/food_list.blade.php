
<div class="alert-order" style="display: flex;justify-content: space-between">
    <div class="img-view" style="margin: 20px">
        <img style="width: 250px;height: 100%" src="{{url("/asset/".$product->price_img)}}" alt="">

    </div>
    <div style="margin-top: 15%;margin-left: 50px;background-color: white">
        <div style="background-color: white" ><h1>Name: {{$product->product_name}}</h1></div>
        <div style="margin-top: 20px;background-color: white"><h1>Price: ${{$product->price}}</h1></div>
        <div class="go-to-card" style="margin-top: 20px"><button type="button" style="padding: 10px;border-radius: 5px;color: white;background-color: black;cursor: pointer">Go to Card</button></div>
    </div>
</div>


<script>
    $(document).ready(function (){
        //Go To Card
        $('.go-to-card').click(function (){
            var bk = '<div class="bk"></div>';

            $('.view-card').css("visibility","hidden");
            $('.view-card-order-content').css("display","block")
        });
    })
</script>
